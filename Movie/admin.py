from django.contrib import admin
from django import forms
# Register your models here.
from Movie.models import *
from sorl.thumbnail import get_thumbnail

BANNER_THUMBS_SIZE_X = '100x150'

class MovieForm(forms.ModelForm):
    class Meta:
        model = Movie
        fields = '__all__'


class MoviePlotInline(admin.TabularInline):
    model = MoviePlot
    extra = 0


class MovieSummaryInline(admin.TabularInline):
    model = MovieTopSummary
    extra = 0


class MoviePersonRelationInline(admin.TabularInline):
    model = MoviePersonRelation
    extra = 0


class MovieWikiLinkInline(admin.TabularInline):
    model = MovieWikiLink
    extra = 0


class MovieGenreInline(admin.TabularInline):
    model = MovieGenre
    extra = 0


class MovieAdmin(admin.ModelAdmin):
    form = MovieForm
    search_fields = ['name']
    list_filter = ['language']
    list_display = ['name', 'image_thumb', 'language']

    inlines = [MovieWikiLinkInline, MovieGenreInline, MoviePersonRelationInline, MovieSummaryInline, MoviePlotInline]

    def image_thumb(self, obj):
        try:
            img = MovieImage.objects.filter(movie=obj)
            crop_direction = 'center'
            if len(img) > 0:
                thumb = get_thumbnail(img[0].image.image, BANNER_THUMBS_SIZE_X, crop=crop_direction)
            else:
                return  "No Images"
            return u'<img src="{0}" />'.format(thumb.url)
        except Exception as e:
            return "Image Error {0}".format(e)

    image_thumb.allow_tags = True
    image_thumb.short_description = 'image'


class PersonForm(forms.ModelForm):
    class Meta:
        model = MoviePerson
        fields = '__all__'


class MoviePersonWikiLinkInline(admin.TabularInline):
    model = MoviePersonWikiLink
    extra = 0


class PersonAdmin(admin.ModelAdmin):
    form = PersonForm
    search_fields = ['name']

    inlines = [MoviePersonWikiLinkInline, ]

admin.site.register(MoviePerson, PersonAdmin)
admin.site.register(Movie, MovieAdmin)
admin.site.register(MovieLanguage)
