from django.core.files.temp import NamedTemporaryFile
from django.template.defaultfilters import slugify
from django.core.files import File
from Movie.models.AllImages import *
import urllib.request

HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding": "gzip,deflate,sdch",
    "Accept-Language": "en-US,en;q=0.8,te;q=0.6",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
}

HEADERS_WALL_SITE = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding": "gzip,deflate,sdch",
    "Accept-Language": "en-US,en;q=0.8,te;q=0.6",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
}


def _get_proper_filename(filename):
    ax = filename.split('.')
    if len(ax) >= 2:
        file_ext = '.' + filename.split('.')[-1]
        slug_filename = slugify(filename.replace(file_ext, '')) + file_ext
    else:
        slug_filename = slugify(filename)
    slug_filename = slug_filename.replace('-', '_')
    return slug_filename


def _get_type_instance(type):
    img_type, created = ImageType.objects.get_or_create(name=type)
    return img_type


class ImageSaveEngine:
    """
    returns an instance of Image and also error code in a tuple
    (Image, StatusCode, Exception)
    """

    def __init__(self, headers=False):
        self.headers = headers

    def save(self, url, type, desc=None):
        return self.save_image_from_url(url, type, desc)

    def save_image_from_url(self, url, type, desc):
        """
        inputs: url,name,user,cateorie[not required]
        creates name for that specific categorie if does not exist
        saves the url's image into that specific name
        returns [-1,error_code,desc,url] on fail
        returns [1,sucess_code,path,url] on sucess
        error codes:
         0 - unknown status check desc
         1 - failed on downloading image
         2 - unable to create name/categorie
         3 - unable to save
         4 - unable to get username
         5 - unable to get file name ??
         100 - sucessfully saved image
        """
        try:
            f = File(self._handle_upload_url_file(url))
        except Exception as e:
            return None, 1, e
        new_image = Image()

        try:
            new_image.type = _get_type_instance(type)
        except Exception as e:
            return None, 2, e

        try:
            new_image.source = url
            filename = url.split('/')[-1]
            slug_filename = _get_proper_filename(filename)
        except Exception as e:
            return None, 5, e

        try:
            if desc is not None:
                new_image.desc = desc
            new_image.image.save(slug_filename, f)
        except Exception as e:
            return None, 3, e

        return new_image, 100, None

    def _handle_upload_url_file(self, url):
        try:
            img_temp = NamedTemporaryFile()
            if self.headers:
                r = urllib.request.Request(url, headers=HEADERS_WALL_SITE)
            else:
                r = urllib.request.Request(url)
            with urllib.request.urlopen(r) as response:
                img_temp.write( response.read())
                img_temp.flush()
        except Exception as e:
            raise e
        return img_temp
