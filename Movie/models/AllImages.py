import os

import datetime
from django.db import models
from Movie.models.MovieBaseModel import MovieBaseModel
from Movie.models.MovieModel import Movie
from Movie.models.MoviePersonModel import MoviePerson


def get_file_path(instance, filename):
    return os.path.join(instance.directory_string_var, filename)


class ImageType(MovieBaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class Image(MovieBaseModel):
    image = models.ImageField(upload_to=get_file_path)
    now = datetime.datetime.now()
    directory_string_var = 'image/%s/%s/%s/' % (now.year, now.month, now.day)
    image_enabled = models.BooleanField(default=True)
    source = models.CharField(max_length=250, blank=True, null=True)
    desc = models.TextField(max_length=512, blank=True, null=True)
    type = models.ForeignKey(ImageType, related_name='%(class)s_type')

    def __str__(self):
        return '{0} - path: {1}'.format(self.type, self.image.path)

    class Meta:
        app_label = "Movie"


class MovieImage(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    image = models.ForeignKey(Image, related_name='%(class)s_image')

    def __str__(self):
        return 'image for {0} of type {1}'.format(self.movie, self.image.type)

    class Meta:
        app_label = "Movie"
