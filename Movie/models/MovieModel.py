from Movie.models.MovieBaseModel import MovieBaseModel
from django.db import models


class MovieLanguage(MovieBaseModel):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class Movie(MovieBaseModel):
    name = models.TextField(max_length=512)
    language = models.ForeignKey(MovieLanguage, related_name='%(class)s_language')
    master_list = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MoviePlot(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    plot = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.movie.name

    class Meta:
        app_label = "Movie"


class MovieTopSummary(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    summary = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.movie.name

    class Meta:
        app_label = "Movie"


class MovieWikiLink(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    wiki_link = models.CharField(max_length=255)

    def __str__(self):
        return "wiki link: {0} for movie {1}".format(self.wiki_link, self.movie)

    class Meta:
        app_label = "Movie"


class MovieAdditionalDetails(MovieBaseModel):
    key = models.CharField(max_length=255)
    details = models.TextField()
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    link = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.movie

    class Meta:
        app_label = "Movie"


class GenreType(MovieBaseModel):
    name = models.CharField(max_length=255)
    href = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MovieGenre(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    genre = models.ForeignKey(GenreType, related_name='%(class)s_genre')

    def __str__(self):
        return '{0} movie genre {1}'.format(self.movie, self.genre)

    class Meta:
        app_label = "Movie"


