from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class MovieBaseModel(models.Model):
    created = models.DateTimeField(verbose_name="Creation Date", auto_now_add=True)
    modified = models.DateTimeField(verbose_name="Modified Date", auto_now=True)

    created_by = models.ForeignKey(User, blank=True, null=True, editable=False, related_name='%(class)s_created_by')
    modified_by = models.ForeignKey(User, blank=True, null=True, editable=False, related_name='%(class)s_modified_by')

    class Meta:
        abstract = True
        app_label = 'movie'

    def save(self, *args, **kwargs):
        """on save update timestamps"""
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(MovieBaseModel, self).save(*args, **kwargs)
