from django.db import models
from Movie.models.MovieBaseModel import MovieBaseModel
from Movie.models.MovieModel import Movie
from Movie.models.MoviePersonModel import MoviePerson


class DateType(MovieBaseModel):
    """
        has date type codes,
        like release date in india
        audio release date
        100th day date
        any other date type code
        birthdate
        award winning date
        marriage date
    """
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MovieDate(MovieBaseModel):
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    date_type = models.ForeignKey(DateType)
    date = models.DateTimeField(verbose_name="Date")
    year_known = models.BooleanField(verbose_name="Only Year is known? ", default=True)
    date_known = models.BooleanField(verbose_name="Date known ", default=True)
    month_known = models.BooleanField(verbose_name='month known? ', default=True)

    def __str__(self):
        return "date type: {0} for movie {1} is {2}".format(self.date_type, self.movie, self.date)

    class Meta:
        app_label = "Movie"


class MoviePersonDate(MovieBaseModel):
    person = models.ForeignKey(MoviePerson, related_name='%(class)s_person')
    date_type = models.ForeignKey(DateType)
    date = models.DateTimeField(verbose_name="Date")

    def __str__(self):
        return "date type: {0} for person {1} is {2}".format(self.date_type, self.person, self.date)

    class Meta:
        app_label = "Movie"
