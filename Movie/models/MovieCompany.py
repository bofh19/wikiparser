from django.db import models

from Movie.models.MovieBaseModel import MovieBaseModel
from Movie.models.MovieModel import Movie
from Movie.models.MoviePersonModel import MoviePerson


class MovieCompany(MovieBaseModel):
    """
    contains various movie company names, can add summary,
    info, dates etc.
    """
    name = models.TextField(max_length=512)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MovieCompanyWikiLink(MovieBaseModel):
    """
    contains movie company wikipedia link
    """
    company = models.ForeignKey(MovieCompany, related_name='%(class)s_company')
    wiki_link = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return '{0} wiki name is {1}'.format(self.company, self.wiki_link)

    class Meta:
        app_label = "Movie"


class MovieCompanyRelationType(MovieBaseModel):
    """
    contains various company movie relation types,
    ex: production, distribution, animation
    """
    name = models.TextField(max_length=512)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MovieCompanyRelation(MovieBaseModel):
    company = models.ForeignKey(MovieCompany, related_name='%(class)s_company')
    movie = models.ForeignKey(Movie, related_name='%(class)s_movie')
    relation = models.ForeignKey(MovieCompanyRelationType, blank=True, null=True, related_name='%(class)s_relation')
    summary = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{0} company for movie {1} on relation {2}'.format(self.company, self.movie, self.relation)

    class Meta:
        app_label = "Movie"


class MovieCompanyPersonRelationType(MovieBaseModel):
    """
    contains various company and person relation types
    ex: founder, ceo, etc.
    """
    name = models.TextField(max_length=512)

    def __str__(self):
        return self.name

    class Meta:
        app_label = "Movie"


class MovieCompanyPerson(MovieBaseModel):
    person = models.ForeignKey(MoviePerson, related_name='%(class)s_person')
    company = models.ForeignKey(MovieCompany, related_name='%(class)s_company')
    relation = models.ForeignKey(MovieCompanyPersonRelationType, null=True, blank=True,
                                 related_name='%(class)s_relation')

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.person, self.company, self.relation)

    class Meta:
        app_label = "Movie"
