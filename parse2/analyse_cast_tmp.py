from bs4 import BeautifulSoup as bs4
from parse2.table_helper import *
from parse2.helper import LOG
import re


def get_link_href_simple(html):
    try:
        soup = bs4(html, 'html.parser')
        out = {}
        out['text'] = soup.get_text()
        out['href'] = ''
        hrefs = soup.find_all('a')
        if len(hrefs) > 1:
            raise Exception("not simple more than 1 href " + html)
        if len(hrefs) == 1:
            out['href'] = hrefs[0]['href'].split('/')[-1]
    except Exception as e:
        LOG("ERROR")
        LOG(html)
        raise e
    return out

def get_data_from_stuff(html_data):
    html_data = html_data.replace('<br/>', ',').replace('<br>', ',')
    html_data = html_data.replace('<small>as</small>', ' as ')
    soup = bs4(html_data, 'html.parser')
    ps = soup.find_all('p')
    lis = soup.find_all('li')
    name_text = ''
    name_href = ''
    char_text = ''
    char_href = ''
    original_text = ''
    other_text = ''
    other_hrefs = []
    multi_chars = False
    multi_names = False
    pt = re.compile("\(.+\)")
    if len(ps) <= 0 and len(lis) <= 0:
        raise Exception("not li or p wtf? or len > 1 len.ps" + str(len(ps)) + " len.lis " + str(len(lis)))

    text = soup.get_text()
    split_char = ' as '
    texts = text.split(' as ')
    if len(texts) <= 1:
        texts = text.split(' - ')
        if len(texts) > 1:
            split_char = ' - '

    original_text = text
    if len(texts) == 1:
        name_text = texts[0]
        contains_bracket = pt.search(html_data)
        # case when it is something ( sometext ) format
        if contains_bracket is None:
            hrefs = soup.find_all('a')
            if len(hrefs) > 1:
                raise Exception('len of hrefs > 1 while len.texts is== 1')
            if len(hrefs) == 1:
                name_href = hrefs[0]['href'].split('/')[-1]
        else:
            bracket_text = contains_bracket.group()
            html_data = html_data.replace(bracket_text, '')
            bracket = bs4(bracket_text, 'html.parser')
            hrefs = bracket.find_all('a')
            other_text = bracket.get_text()
            for href in hrefs:
                other_hrefs.append(href['href'].split('/')[-1])

            html_data_new_soup = bs4(html_data, 'html.parser')
            hrefs = html_data_new_soup.find_all('a')
            if len(hrefs) > 1:
                raise Exception('len of hrefs > 1 while len.texts is== 1')
            if len(hrefs) == 1:
                name_href = hrefs[0]['href'].split('/')[-1]
    elif len(texts) == 2:
        name_text = texts[0]
        char_text = texts[1]
        html_datas = html_data.split(split_char)
        sp1 = bs4(html_datas[0], 'html.parser')
        hrefs = sp1.find_all('a')
        print(html_datas)

        if len(hrefs) > 1:
            LOG("len of hrefs > 1 while it should be in 1 or 0")
            # multi names check
            html_datas[0] = html_datas[0].replace(' and ', ' / ')
            names_split = html_datas[0].split(' / ')
            if len(names_split) <= 1:
                raise Exception("len of hrefs > 1 while it should be in 1 or 0 and no multi chars")
            else:
                multi_names = True
                all_names = []
                for en_sp in names_split:
                    text_href = get_link_href_simple(str(en_sp))
                    all_names.append({'text': text_href['text'], 'href': text_href['href']})

        if len(hrefs) == 1:
            name_href = hrefs[0]['href'].split('/')[-1]
        # check for multiple characters
        chars_split = html_datas[1].split(' / ')
        if len(chars_split) <= 1:
            chars_split = html_datas[1].split(' or ')
        if len(chars_split) >= 2:
            multi_chars = True
            all_chars = []
            for ec_split in chars_split:
                text_href = get_link_href_simple(str(ec_split))
                all_chars.append({'text': text_href['text'], 'href': text_href['href']})
        else:
            sp2 = bs4(html_datas[1], 'html.parser')
            hrefs = sp2.find_all('a')
            if len(hrefs) > 1:
                raise Exception("len of hrefs in char is > 1 while it should be 1 or 0")
            if len(hrefs) == 1:
                char_href = hrefs[0]['href'].split('/')[-1]
    else:
        raise Exception("texts length is 0 or > 2 len.texts:" + str(len(texts)))

    out_obj = {}
    if multi_names:
        out_obj['names'] = all_names
    else:
        out_obj['names'] = [{'text': name_text, 'href': name_href}]
    if not multi_chars:
        out_obj['chars'] = [{'text': char_text, 'href': char_href}]
    else:
        out_obj['chars'] = all_chars
    out_obj['all_text'] = original_text
    out_obj['other'] = {'text': other_text, 'href': other_hrefs}
    return out_obj