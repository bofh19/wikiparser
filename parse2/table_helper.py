__author__ = 'skarumuru'

def table_to_dict(table):
    #result = defaultdict(lambda: defaultdict(unicode))
    result = defaultdict(lambda: defaultdict())
    for row_i, row in enumerate(table.find_all('tr')):
        for col_i, col in enumerate(row.find_all('td')):
            colspan = 1
            rowspan = 1
            try:
                colspan = int(col['colspan'])
            except Exception as e:
                pass
            try:
                rowspan = int(col['rowspan'])
            except Exception as e:
                pass
            col_data = str(col)
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    result[i][j] = col_data
    return result


from collections import defaultdict


def table_to_dict_th(table):
    #result = defaultdict(lambda: defaultdict(unicode))
    result = defaultdict(lambda: defaultdict())
    for row_i, row in enumerate(table.find_all('tr')):
        for col_i, col in enumerate(row.find_all('th')):
            colspan = 1
            rowspan = 1
            try:
                colspan = int(col['colspan'])
            except Exception as e:
                pass
            try:
                rowspan = int(col['rowspan'])
            except Exception as e:
                pass
            col_data = str(col)
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    if (colspan > 1):
                        result[i][j] = col_data + "_col_" + str(j)
                    else:
                        result[i][j] = col_data
    return result


def iter_2d_dict(dct):
    for i, row in sorted(dct.items()):
        cols = []
        for j, col in sorted(row.items()):
            cols.append(col)
        yield cols


def get_table_as_list(table, th=False):
    if (th):
        return list(iter_2d_dict(table_to_dict_th(table)))
    else:
        return list(iter_2d_dict(table_to_dict(table)))
