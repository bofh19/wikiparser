from urllib.request import urlopen
import json

from bs4 import UnicodeDammit

url = "http://en.wikipedia.org/w/api.php?action=parse&format=json&page=Tamanna_Bhatia&redirects=&prop=text"


def get_link_data(wlink):
    # returns is_error?, is_redirect?, redirect_links, data
    link_title = wlink.lstrip().rstrip().replace(' ', '_')
    url = "http://en.wikipedia.org/w/api.php?action=parse&format=json&page=" + link_title + "&redirects=&prop=text"
    f = urlopen(url)
    data = f.read().decode('utf8')
    json_data = json.loads(data)
    try:
        error = json_data['error']
        return True, False, None, None
    except Exception as e:
        # no errors
        pass
    redirect_to = ''
    is_redirect = False
    try:
        for redirect in json_data['parse']['redirects']:
            redirect_to += redirect['to'].replace(' ', '_')
        is_redirect = True
    except Exception as e:
        # no redirects
        pass

    # no redirects will occur here
    raw_data = json_data['parse']['text']['*']
    dammit = UnicodeDammit(raw_data)
    htext = dammit.unicode_markup
    return False, is_redirect, redirect_to, htext
