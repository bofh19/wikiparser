from Movie.models import *
import datetime
import re

from Movie.utils.image_save_helper import ImageSaveEngine


def __add_additional_details(key, text, href, db_m):
    if text is not None:
        text = text.lstrip().rstrip()
    if text is None or len(text) <= 0:
        return
    ma = MovieAdditionalDetails()
    ma.key = key
    ma.details = text.replace(u'\xa0', ' ')
    ma.link = href
    ma.movie = db_m
    ma.save()


def __add_movie_person_with_type(txt, href, rel_name, db_m):
    if txt is not None:
        txt = txt.lstrip().rstrip()
    if txt is None or len(txt) <= 0:
        return
    if href is not None and len(href) > 0:
        try:
            found_href = MoviePersonWikiLink.objects.get(wiki_link=href)
            txt = found_href.person.name
        except Exception as e:
            pass
    per, per_created = MoviePerson.objects.get_or_create(name=txt)
    rel_type, rel_created = MoviePersonRelationType.objects.get_or_create(name=rel_name)
    if href is not None and len(href) > 0:
        per_link, pl_created = MoviePersonWikiLink.objects.get_or_create(person=per, wiki_link=href)
    mpr, mpr_created = MoviePersonRelation.objects.get_or_create(person=per, relation=rel_type, movie=db_m)
    return mpr, per


def handle_cast_cast(data, db_m):
    names = data['names']
    chars = data['chars']
    if len(names) <= 0:
        return
    name = data['names'][0]
    if len(names) > 1:
        for nx in names:
            if 'telugu' in nx['text'].lower():
                name = nx
    if len(name['text']) > 0:
        mpr, per = __add_movie_person_with_type(name['text'], name['href'], 'Cast', db_m)
        for ch in chars:
            if len(ch['text']) > 0:
                mpc, mpc_created = MoviePersonCharacter.objects.get_or_create(relation=mpr, name=ch['text'])
                if mpc_created:
                    if len(ch['href']) > 0:
                        mpc.link = ch['href']
                mpc.save()


def handle_cast_image(data, db_m, db_save=True):
    x = ImageSaveEngine()
    desc = data['text']
    if data['images'] is not None:
        for ed in data['images']:
            href = ed
            if 'http:' not in href and 'https:' not in href:
                href = 'http:' + href
            img, stat, e = x.save(href, 'cast_image', desc=desc)
            if img is not None:
                mi = MovieImage()
                mi.movie = db_m
                mi.image = img
                mi.save()

