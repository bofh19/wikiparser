from Movie.models import *
import datetime
import re


def handle_main_table_key_movie(data, db_m):
    """
    ['movie', [ { 'text': '' , 'href': '' } ]
    :param data:
    :param db_m:
    :return:
    """
    if data is not None and len(data) > 0:
        data = data[0]
        txt = data['text']
        href = data['href']

        if href is not None and len(href) > 0:
            db_link, created = MovieWikiLink.objects.get_or_create(movie=db_m, wiki_link=href)
            db_link.save()


def handle_main_table_key_produced_by(data, db_m):
    """
    'produced_by', [ { 'text': '' , 'href': '' } ]
    :param data:
    :param db_m:
    :return:
    """
    if data is not None and len(data) > 0:
        for ed in data:
            __add_movie_person_with_type(ed, 'Producer', db_m)


def handle_main_table_key_id(data, db_m):
    pass


def handle_main_table_key_release_date(data, db_m, db_save=True):
    """
    'release_date', // regex and stuff yyyy or d-m-y,, d/mmm/yyyy, { month: '' , year: '', date: '' }
    :param db_save:
    :param data:
    :param db_m:
    :return:
    """
    date_type, created = DateType.objects.get_or_create(name="Release Date")
    md = MovieDate()
    if type(data) is str or type(data) is int:
        try:
            int_data = int(data)
            md.date = datetime.datetime.strptime("01 Jan {0}".format(int_data), "%d %b %Y")
            md.date_known = False
            md.month_known = False
        except Exception as e:
            if '-' in data:
                md.date = datetime.datetime.strptime(data, "%d-%m-%Y")
            elif '/' in data:
                try:
                    md.date = datetime.datetime.strptime(data, '%d/%b/%Y')
                except Exception as e:
                    md.date = datetime.datetime.strptime(data, "%d/%m/%Y")
    else:
        month = data['month']
        year = data['year']
        date = data['date']
        date_comp = re.compile(r'[^\d]+')
        date = date_comp.sub('', date)
        md.date = datetime.datetime.strptime("{0}-{1}-{2}".format(date, month.title(), year), "%d-%b-%Y")
    if db_save:
        md.date_type = date_type
        md.movie = db_m
        if md.date is not None:
            md.save()
        else:
            __add_additional_details('release_date_error', str(data), '', db_m)
    else:
        return md.date


def handle_main_table_key_cast(data, db_m):
    if data is not None and len(data) > 0:
        for ed in data:
            __add_movie_person_with_type(ed, 'Cast', db_m)


def handle_main_table_key_genre(data, db_m):
    """
    'genre',
        - [ { 'text': '' , 'href': '' } ]
        // ignore all that can be casted to numbers,
        // some can be spliced by /
    :param data:
    :param db_m:
    :return:
    """
    if data is not None and len(data) > 0:
        for ed in data:
            txt = ed['text']
            href = ed['href']
            if txt is None or len(txt) <= 0:
                continue
            if href is None or len(href) <= 0:
                href = None
            genre, created = GenreType.objects.get_or_create(name=txt, href=href)
            mg, created = MovieGenre.objects.get_or_create(movie=db_m, genre=genre)


def handle_main_table_key_add_notes(data, db_m):
    """
    'additional_notes',
        - just a string always
        - can contain year of release date
        - can say " some text Produced By < company name > "
        - can say Music <space> name
        - can say Musical Hit by <name>
        - can say "some text production from <name>
        - can say Music: <name>
        - can say Music: <name>, Lyrics:<name>
        - can say Music: <name>, <ignore text>
    """
    try:
        int_date = int(data)
        handle_main_table_key_release_date(data, db_m)
    except Exception as e:
        if 'Produced By' in data:
            data = data.split('Produced By')
            data = data[1]
            rx = re.compile(r'\[\d+\]')
            data = re.sub(rx, '', data)
            if not any(x in data for x in ['Satellite', 'Produced by']):
                handle_main_table_key_produced_by([{'text': data, 'href': ''}], db_m)
        elif 'Musical Hit by ' in data:
            data = data.split('Musical Hit by ')
            data = data[1]
            __add_movie_person_with_type({'text': data, 'href': ''}, 'Music', db_m)
        elif 'Music: ' in data:
            data = data.split('Music: ')
            data = data[1]
            data = data.split(',')
            if not any(x in data[0] for x in ['Story', 'Lyrics', 'Hollywood', 'Dialogues']):
                __add_movie_person_with_type({'text': data[0], 'href': ''}, 'Music', db_m)

        ma = MovieAdditionalDetails()
        ma.key = "additional_details"
        ma.details = data
        ma.movie = db_m
        ma.save()


def handle_main_table_key_director(data, db_m):
    if data is not None and len(data) > 0:
        for ed in data:
            __add_movie_person_with_type(ed, 'Director', db_m)


def handle_main_table_key_music_director(data, db_m):
    if data is not None and len(data) > 0:
        for ed in data:
            __add_movie_person_with_type(ed, 'Music', db_m)


def __add_movie_person_with_type(ed, rel_name, db_m):
    txt = ed['text']
    href = ed['href']
    if txt is not None:
        txt = txt.lstrip().rstrip()
    if txt is None or len(txt) <= 0:
        return
    if href is not None and len(href) > 0:
        try:
            found_href = MoviePersonWikiLink.objects.get(wiki_link=href)
            txt = found_href.person.name
        except Exception as e:
            pass
    per, per_created = MoviePerson.objects.get_or_create(name=txt)
    rel_type, rel_created = MoviePersonRelationType.objects.get_or_create(name=rel_name)
    if href is not None and len(href) > 0:
        per_link, pl_created = MoviePersonWikiLink.objects.get_or_create(person=per, wiki_link=href)
    mpr, mpr_created = MoviePersonRelation.objects.get_or_create(person=per, relation=rel_type, movie=db_m)

def __add_additional_details(key, text, href, db_m):
    if text is not None:
        text = text.lstrip().rstrip()
    if text is None or len(text) <= 0:
        return
    ma = MovieAdditionalDetails()
    ma.key = key
    ma.details = text.replace(u'\xa0', ' ')
    ma.link = href
    ma.movie = db_m
    ma.save()

def handle_infobox_missing_keys(data, db_m):
    __add_additional_details('maintable_missing', str(data), '', db_m )

def get_main_table_handles():
    obj = {}
    obj['movie'] = handle_main_table_key_movie
    obj['produced_by'] = handle_main_table_key_produced_by
    obj['id'] = handle_main_table_key_id
    obj['release_date'] = handle_main_table_key_release_date
    obj['cast'] = handle_main_table_key_cast
    obj['genre'] = handle_main_table_key_genre
    obj['additional_notes'] = handle_main_table_key_add_notes
    obj['director'] = handle_main_table_key_director
    obj['music_director'] = handle_main_table_key_music_director
    obj['missing_keys'] = handle_infobox_missing_keys
    return obj
