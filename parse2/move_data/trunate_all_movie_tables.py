# MovieModel
from Movie.models.MovieModel import MovieLanguage
from Movie.models.MovieModel import Movie
from Movie.models.MovieModel import MovieTopSummary
from Movie.models.MovieModel import MoviePlot
from Movie.models.MovieModel import MovieWikiLink
from Movie.models.MovieModel import MovieAdditionalDetails
from Movie.models.MovieModel import GenreType
from Movie.models.MovieModel import MovieGenre

# MovieDateModel
from Movie.models.AllDatesModel import DateType
from Movie.models.AllDatesModel import MovieDate
from Movie.models.AllDatesModel import MoviePersonDate

# MoviePersonModel
from Movie.models.MoviePersonModel import MoviePerson
from Movie.models.MoviePersonModel import AlternateName
from Movie.models.MoviePersonModel import MoviePersonRelationType
from Movie.models.MoviePersonModel import MoviePersonRelation
from Movie.models.MoviePersonModel import MoviePersonWikiLink
from Movie.models.MoviePersonModel import MoviePersonCharacter

# Movie Company
from Movie.models.MovieCompany import MovieCompany
from Movie.models.MovieCompany import MovieCompanyPerson
from Movie.models.MovieCompany import MovieCompanyPersonRelationType
from Movie.models.MovieCompany import MovieCompanyRelation
from Movie.models.MovieCompany import MovieCompanyRelationType
from Movie.models.MovieCompany import MovieCompanyWikiLink

#MovieImage
from Movie.models.AllImages import ImageType
from Movie.models.AllImages import Image
from Movie.models.AllImages import MovieImage

def truncate_all():
    arr = [
        MovieCompanyRelation
        , MovieCompanyRelationType
        , MovieCompanyPerson
        , MovieCompanyPersonRelationType
        , MovieCompanyWikiLink
        , MovieCompany
        #
        , MoviePersonCharacter
        , MoviePersonWikiLink
        , MoviePersonRelation
        , MoviePersonRelationType
        , AlternateName
        , MovieDate
        , MoviePersonDate
        , DateType
        , MoviePerson
        #
        , MovieImage
        , Image
        , ImageType
        #
        , MovieGenre
        , GenreType
        , MovieAdditionalDetails
        , MovieWikiLink
        , MoviePlot
        , MovieTopSummary
        , MovieLanguage
        , Movie
    ]
    for x in arr:
        x.objects.all().delete()
