from parse2.move_data.move_data_infobox import get_info_box_handles
from parse2.models import Movie as p2Movie
from parse2.analyse_movie_top_summary_plot import get_plot_for_movie
from parse2.analyse_movie_top_summary_plot import get_top_summary_for_movie
from parse2.analyse_infobox import get_movie_info_for_movie
from parse2.analyse_movie_cast import get_cast_for_movie
from parse2.analyse_table_cols import get_main_table_info
from parse2.move_data.move_data_main_table import get_main_table_handles
from parse2.move_data.move_data_cast import handle_cast_cast
from parse2.move_data.move_data_cast import handle_cast_image
from Movie.models import *
from parse2.helper import LOG

__author__ = 'skarumuru'


def move_this_movie(m, lang):
    LOG("Moving {0}".format(m.id))
    summary = get_top_summary_for_movie(m)
    plot = get_plot_for_movie(m)
    infobox = get_movie_info_for_movie(m)
    cast = get_cast_for_movie(m)
    main_table = get_main_table_info(m)
    found_movie = None
    if m.wiki_link is not None and len(m.wiki_link) > 0:
        try:
            found_movie = MovieWikiLink.objects.get(wiki_link=m.wiki_link)
            found_movie = found_movie.movie.name
        except Exception as e:
            found_movie = m.movie_name
    else:
        found_movie = m.movie_name
    db_m, dbm_created = Movie.objects.get_or_create(name=found_movie, language=lang)
    LOG("Movie Created ? {0} current id {1}".format(dbm_created, db_m.id))
    if dbm_created:
        db_m.name = m.movie_name
        db_m.language = lang
        db_m.master_list = m.master_list_name
        db_m.save()

        if m.wiki_link is not None and len(m.wiki_link) > 0:
            dbm_link, created = MovieWikiLink.objects.get_or_create(movie=db_m, wiki_link=m.wiki_link)

    db_m.movieplot_movie.all().delete()
    for p in plot:
        db_plot = MoviePlot()
        db_plot.movie = db_m
        db_plot.plot = p
        db_plot.save()

    db_m.movietopsummary_movie.all().delete()
    for s in summary:
        db_sum = MovieTopSummary()
        db_sum.movie = db_m
        db_sum.summary = s
        db_sum.save()

    main_table_handlers = get_main_table_handles()
    for each_key in main_table.keys():
        if each_key.lower() not in main_table_handlers.keys():
            val = each_key + ':' + str(main_table[each_key])
            head = 'missing_keys'
            main_table_handlers[head.lower()](val, db_m)
        else:
            LOG(str(main_table_handlers[each_key.lower()]))
            main_table_handlers[each_key.lower()](main_table[each_key], db_m)

    info_table_handlers = get_info_box_handles()
    for eb in infobox:
        details = eb['details']
        for ed in details:
            head = ' '.join([x['text'] for x in ed['heading']])
            if head.lower() not in info_table_handlers.keys():
                val = head + ':' + str(ed['values'])
                head = 'missing_keys'
                info_table_handlers[head.lower()](val, db_m)
            else:
                LOG(str(info_table_handlers[head.lower()]))
                info_table_handlers[head.lower()](ed['values'], db_m)

    if cast is not None and len(cast) > 0:
        for each_cast in cast:
            for each_ob in each_cast['data']['casts']:
                handle_cast_cast(each_ob, db_m)
            if each_cast['images'] is not None:
                handle_cast_image(each_cast['images'], db_m)




def move_all_telugu_movies():
    ms = p2Movie.objects.filter(language='telugu')
    lang, created = MovieLanguage.objects.get_or_create(name="telugu")

    for m in ms:
        move_this_movie(m, lang)

def move_all_tamil_movies():
    ms = p2Movie.objects.filter(language='tamil').order_by('-id')
    lang, created = MovieLanguage.objects.get_or_create(name='tamil')

    for m in ms:
        move_this_movie(m, lang)

def cast_check():
    for m in p2Movie.objects.filter(language='telugu'):
        cat = get_cast_for_movie(m)
        # if len(cat) > 1:
        #     print('1',m.id,m.movie_name,cat)
        if len(cat) > 0:
            for ec in cat:
                data = ec['data']
                for c in data['casts']:
                    # if len(c['chars']) > 1 :
                    #     print ('2',m.id,m.movie_name, c)
                    if len(c['names']) > 1:
                        print('3', m.id, m.movie_name, c)
                        # if c['other'] is not None:
                        #     if len(c['other']['text']) > 0:
                        #         print('4',m.id, m.movie_name, c)
                if len(ec['additional']) is not None and len(ec['additional']) > 0:
                    print('5', m.id, m.movie_name, ec['additional'])
                if ec['images'] is not None and len(ec['images']) > 0:
                    print('6', m.id, m.movie_name, ec['images'])
