from Movie.models import *
import datetime
import re

from Movie.utils.image_save_helper import ImageSaveEngine

"""
data will always contain the values part of infobox array
heading is the key, has to be taken care of super function
assumes data is not null and data.text is not null or empty
"""


def handle_info_box_created_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Created By', db_m)


def handle_info_box_presented_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Presented By', db_m)


def handle_info_box_cam_setup(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Camera Setup', db_m)


def handle_info_box_editors(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Editor', db_m)


def handle_info_box_label(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('label', ea['text'], ea['href'], db_m)


def handle_info_box_directed_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Director', db_m)


def handle_info_box_country(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('country', ea['text'], ea['href'], db_m)


def handle_info_box_characters(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Cast', db_m)


def handle_info_box_edited_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Editor', db_m)


def handle_info_box_box_office(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('box_office', ea['text'], ea['href'], db_m)


def handle_info_box_first_shown_in(data, db_m, db_save=True):
    # ignroed for now
    pass


def handle_info_box_length(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('length', ea['text'], ea['href'], db_m)


def handle_info_box_original_langs(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('original_language', ea['text'], ea['href'], db_m)


def handle_info_box_box_image(data, db_m, db_save=True):
    x = ImageSaveEngine()
    for ed in data:
        href = ed['href']
        if 'http:' not in href and 'https:' not in href:
            href = 'http:' + href
        img, stat, e = x.save(href, 'box_image')
        if img is not None:
            mi = MovieImage()
            mi.movie = db_m
            mi.image = img
            mi.save()


def handle_info_box_country_origin(data, db_m, db_save=True):
    # ignored for now
    pass


def handle_info_box_subject(data, db_m, db_save=True):
    for ed in data:
        mts = MovieTopSummary()
        mts.movie = db_m
        mts.summary = ed['text']
        mts.save()


def handle_info_box_composers(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Composer', db_m)


def handle_info_box_release_dates(data, db_m, db_save=True):
    date_type, created = DateType.objects.get_or_create(name="Release Date")
    values = ' '.join([x['text'] for x in data])
    try:
        date, dk, mk, yk = get_date_from_str(values)
        md = MovieDate()
        md.date_type = date_type
        md.movie = db_m
        md.date = date
        md.year_known = yk
        md.date_known = dk
        md.month_known = mk
        md.save()
    except Exception:
        for ea in data:
            __add_additional_details('release_date_failed', ea['text'], ea['href'], db_m)


def handle_info_box_released(data, db_m, db_save=True):
    handle_info_box_release_dates(data, db_m)


def handle_info_box_place_premiered(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('place_premiered', ea['text'], ea['href'], db_m)


def handle_info_box_original_network(data, db_m, db_save=True):
    for ea in data:
        __add_movie_company_with_type(ea['text'], ea['href'], 'Original Network', db_m)


def handle_info_box_date_premiered(data, db_m, db_save=True):
    handle_info_box_release_dates(data, db_m)


def handle_info_box_executive_producers(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Producer', db_m)


def handle_info_box_producers(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Producer', db_m)


def handle_info_box_language(data, db_m, db_save=True):
    # ignored for now
    pass


def handle_info_box_distributed_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_company_with_type(ea['text'], ea['href'], 'Distributor', db_m)


def handle_info_box_running_time(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('running_time', ea['text'], ea['href'], db_m)


def handle_info_box_box_title(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('box_title', ea['text'], ea['href'], db_m)


def handle_info_box_screenplay(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Screen Play', db_m)


def handle_info_box_voices(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Voice', db_m)


def handle_info_box_narrated_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Narrated By', db_m)


def handle_info_box_starring(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Cast', db_m)


def handle_info_box_recorded(data, db_m, db_save=True):
    # ignored as this is only for 1 and that's also form sound track
    pass


def handle_info_box_written_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Writer', db_m)


def handle_info_box_original_release(data, db_m, db_save=True):
    # ignored for now
    pass


def handle_info_box_also_known_as(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('add_name', ea['text'], ea['href'], db_m)


def handle_info_box_budget(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('box_budget', ea['text'], ea['href'], db_m)


def handle_info_box_audio_format(data, db_m, db_save=True):
    # only doing for this movie rest all ignored
    if db_m.name == 'Comedy Express':
        for ea in data:
            __add_movie_person_with_type(ea['text'], ea['href'], 'Music', db_m)


def handle_info_box_original_language(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('orig_lang', ea['text'], ea['href'], db_m)


def handle_info_box_production_company(data, db_m, db_save=True):
    for ea in data:
        __add_movie_company_with_type(ea['text'], ea['href'], 'Distributor', db_m)


def handle_info_box_cinematography(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Cinematography', db_m)


def handle_info_box_story_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Story By', db_m)


def handle_info_box_produced_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Producer', db_m)


def handle_info_box_music_by(data, db_m, db_save=True):
    for ea in data:
        __add_movie_person_with_type(ea['text'], ea['href'], 'Music', db_m)


def handle_info_box_genre(data, db_m, db_save=True):
    if data is not None and len(data) > 0:
        for ed in data:
            txt = ed['text']
            href = ed['href']
            if txt is None or len(txt) <= 0:
                continue
            if href is None or len(href) <= 0:
                href = None
            genre, created = GenreType.objects.get_or_create(name=txt, href=href)
            mg, created = MovieGenre.objects.get_or_create(movie=db_m, genre=genre)


def handle_info_box_based_on(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('based_on', ea['text'], ea['href'], db_m)


def handle_info_box_locations(data, db_m, db_save=True):
    for ea in data:
        __add_additional_details('locations', ea['text'], ea['href'], db_m)


def handle_info_box_distributor(data, db_m, db_save=True):
    for ea in data:
        __add_movie_company_with_type(ea['text'], ea['href'], 'Distributor', db_m)


def __add_movie_company_with_type(txt, href, rel_name, db_m):
    if txt is not None:
        txt = txt.lstrip().rstrip()
    if txt is None or len(txt) <= 0:
        return
    if href is not None or len(href) > 0:
        try:
            found_href = MovieCompanyWikiLink.objects.get(wiki_link=href)
            txt = found_href.company.name
        except Exception as e:
            pass
    cmp, cmp_created = MovieCompany.objects.get_or_create(name=txt)
    rel_type, rel_created = MovieCompanyRelationType.objects.get_or_create(name=rel_name)
    if href is not None and len(href) > 0:
        cmp_link, cmp_created = MovieCompanyWikiLink.objects.get_or_create(company=cmp, wiki_link=href)

    cmp_rel, cmp_rel_created = MovieCompanyRelation.objects.get_or_create(company=cmp, movie=db_m, relation=rel_type)


def __add_movie_person_with_type(txt, href, rel_name, db_m):
    if txt is not None:
        txt = txt.lstrip().rstrip()
    if txt is None or len(txt) <= 0:
        return
    if href is not None and len(href) > 0:
        try:
            found_href = MoviePersonWikiLink.objects.get(wiki_link=href)
            txt = found_href.person.name
        except Exception as e:
            pass
    per, per_created = MoviePerson.objects.get_or_create(name=txt)
    rel_type, rel_created = MoviePersonRelationType.objects.get_or_create(name=rel_name)
    if href is not None and len(href) > 0:
        per_link, pl_created = MoviePersonWikiLink.objects.get_or_create(person=per, wiki_link=href)
    mpr, mpr_created = MoviePersonRelation.objects.get_or_create(person=per, relation=rel_type, movie=db_m)


def __add_additional_details(key, text, href, db_m):
    if text is not None:
        text = text.lstrip().rstrip()
    if text is None or len(text) <= 0:
        return
    ma = MovieAdditionalDetails()
    ma.key = key
    ma.details = text.replace(u'\xa0', ' ')
    ma.link = href
    ma.movie = db_m
    ma.save()

def handle_infobox_missing_keys(data, db_m):
    __add_additional_details('infobox_missing', str(data), '', db_m )


def get_info_box_handles():
    obj = {'Created by': handle_info_box_created_by,
           'Presented by': handle_info_box_presented_by,
           'Camera setup': handle_info_box_cam_setup,
           'Editor(s)': handle_info_box_editors,
           'Label': handle_info_box_label,
           'Directed by': handle_info_box_directed_by,
           'Country': handle_info_box_country,
           'Characters': handle_info_box_characters,
           'Edited by': handle_info_box_edited_by,
           'Box office': handle_info_box_box_office,
           'First shown in': handle_info_box_first_shown_in,
           'Length': handle_info_box_length,
           'Original language(s)': handle_info_box_original_langs,
           'box_image': handle_info_box_box_image,
           'Country of origin': handle_info_box_country_origin,
           'Subject': handle_info_box_subject,
           'Composer(s)': handle_info_box_composers,
           'Release dates': handle_info_box_release_dates,
           'Executive producer(s)': handle_info_box_executive_producers,
           'Released': handle_info_box_released,
           'Producer': handle_info_box_producers,
           'Language': handle_info_box_language,
           'Distributed by': handle_info_box_distributed_by,
           'Running time': handle_info_box_running_time,
           'box_title': handle_info_box_box_title,
           'Screenplay by': handle_info_box_screenplay,
           'Voices of': handle_info_box_voices,
           'Place premiered': handle_info_box_place_premiered,
           'Narrated by': handle_info_box_narrated_by,
           'Starring': handle_info_box_starring,
           'Original network': handle_info_box_original_network,
           'Recorded': handle_info_box_recorded,
           'Written by': handle_info_box_written_by,
           'Original release': handle_info_box_original_release,
           'Producer(s)': handle_info_box_producers,
           'Also known as': handle_info_box_also_known_as,
           'Budget': handle_info_box_budget,
           'Audio format': handle_info_box_audio_format,
           'Original language': handle_info_box_original_language,
           'Production company': handle_info_box_production_company,
           'Cinematography': handle_info_box_cinematography,
           'Story by': handle_info_box_story_by,
           'Produced by': handle_info_box_produced_by,
           'Music by': handle_info_box_music_by,
           'Genre': handle_info_box_genre,
           'Date premiered': handle_info_box_date_premiered,
           'Based on': handle_info_box_based_on,
           'Location(s)': handle_info_box_locations,
           'Distributor': handle_info_box_distributor,
           'missing_keys': handle_infobox_missing_keys}
    out = {}
    for ea in obj.keys():
        out[ea.lower()] = obj[ea]
    return out


def date_abbr():
    a = {
        'january': 'Jan',
        'february': 'Feb',
        'march': 'Mar',
        'april': 'Apr',
        'may': 'May',
        'june': 'Jun',
        'july': 'Jul',
        'august': 'Aug',
        'september': 'Sep',
        'october': 'Oct',
        'november': 'Nov',
        'december': 'dec'
    }
    out = {}
    for ea in a.keys():
        out[ea.lower()] = a[ea].title()
    return out


def get_date_from_str(dts):
    """
    :param dts:
    :return: datetime_object, day?, month?, year?
    """
    dts = dts.lstrip().rstrip()
    dts = dts.replace(u'\xa0', ' ')
    r = re.compile(r'\[\d+\]')
    dts = re.sub(r, '', dts)
    dts = str(dts.lower())
    mon = date_abbr()
    for m in mon.keys():
        dts = dts.replace(m, mon[m])

    dts = dts.replace('st', '')
    dts = dts.replace('th', '')

    dts = dts.title()
    # year
    try:
        int_dts = int(dts)
        if len(str(int_dts)) >= 4:
            dts = datetime.datetime.strptime('01 Jan {0}'.format(int_dts), "%d %b %Y")
            return dts, False, False, True
        else:
            raise RuntimeError("WTF")
    except ValueError:
        pass

    # (23-05-1999)
    d_pat_1 = re.compile(r'\(\d\d?\-\d\d?\-\d\d\d\d\)')
    found = d_pat_1.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = dts.replace("(", '').replace(')', '')
        dts = datetime.datetime.strptime(dts, '%d-%m-%Y')
        return dts, True, True, True

    # (1936-02-29)
    d_pat_1_1 = re.compile(r'\(\d\d\d\d\-\d\d?\-\d\d?\)')
    found = d_pat_1_1.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = dts.replace("(", '').replace(')', '')
        dt_check = dts.split("-")
        if int(dt_check[1]) <= 12:
            dts = datetime.datetime.strptime(dts, '%Y-%m-%d')
        else:
            dts = datetime.datetime.strptime(dts, '%Y-%d-%m')
        return dts, True, True, True

    # (1932)
    d_pat_2 = re.compile(r'\(\d\d\d\d\)')
    found = d_pat_2.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = dts.replace('(', '').replace(')', '')
        dts = datetime.datetime.strptime('01 Jan {0}'.format(dts), "%d %b %Y")
        return dts, False, False, True

    # 12 Aug 1965 or 1 Aug 1965
    d_pat_3 = re.compile(r'\d\d?\s[A-Z][a-z][a-z]\s\d\d\d\d')
    found = d_pat_3.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = datetime.datetime.strptime(dts, "%d %b %Y")
        return dts, True, True, True

    # Jan 5 1962
    d_pat_4 = re.compile(r'[A-Z][a-z][a-z]\s\d\d?\s\d\d\d\d')
    found = d_pat_4.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = datetime.datetime.strptime(dts, "%b %d %Y")
        return dts, True, True, True

    # 24-11-1967
    d_pat_5 = re.compile(r'\d\d?\-\d\d?\-\d\d\d\d')
    found = d_pat_5.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = datetime.datetime.strptime(dts, "%d-%m-%Y")
        return dts, True, True, True

    # Jan 1983
    d_pat_6 = re.compile(r'[A-Z][a-z][a-z]\s\d\d\d\d')
    found = d_pat_6.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = datetime.datetime.strptime('01 {0}'.format(dts), "%d %b %Y")
        return dts, False, True, True

    # 1986-08-10
    d_pat_7 = re.compile(r'\d\d\d\d\-\d\d?\-\d\d?')
    found = d_pat_7.findall(dts)
    if len(found) > 0:
        dts = found[0]
        dts = datetime.datetime.strptime(dts, "%Y-%m-%d")
        return dts, True, True, True

    return None, False, False, False


def test_dates(h='Release dates'):
    from parse2.models import Movie as p2Movie
    from parse2.analyse_infobox import get_movie_info_for_movie
    ms = p2Movie.objects.filter(language='telugu')
    for m in ms:
        boxes = get_movie_info_for_movie(m)
        for eb in boxes:
            details = eb['details']
            for ed in details:
                head = ' '.join([x['text'] for x in ed['heading']])
                if head == h:
                    values = ' '.join([x['text'] for x in ed['values']])
                    print(m.id, values, ed)
                    dt, dx, mx, yx = get_date_from_str(values)
                    if dt is None:
                        raise Exception("CRASH")
