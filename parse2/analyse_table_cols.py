__author__ = 'skarumuru'

"""
    this file analyses the main table columns to get
    all stuff organized
"""

"""
    {'col_heading': 'Title'} -- movie title  [x]
    {'col_heading': 'Director'} -- director [x]
    {'col_heading': 'Cast'} -- cast [x]
    {'col_heading': 'Genre'} -- genre [x]
    {'col_heading': 'Release'} -- release [x]

    {'col_heading': 'INVALID_OR_UNKNOWN'} -- year of release ? from data

    {'col_heading': 'Notes'} -- depends on what the nodes says [x]
    {'col_heading': 'Release Date'} -- release date in multiple formats [x]

    {'col_heading': 'Source'} -  can be ignored

    {'col_heading': 'Music Director'} - direct names, might have to proper mapping [x]
    {'col_heading': 'Music'} -  same as above [x]
    {'col_heading': 'Sources'} - can be ignored
    {'col_heading': 'Opening_col_0'} - release date month [x]
    {'col_heading': 'Opening_col_1'} - release date- have to get top level table list to get year [x]
    {'col_heading': 'Producer'} - producer [x]
    {'col_heading': '_col_0'}  - release date month [x]
    {'col_heading': '_col_1'} - release date day [x]

    {'col_heading': 'Ref'} - can be ignored

    {'col_heading': 'Produced by'} - produced by name, need proper parsing to map [x]
    {'col_heading': 'Starting_col_0'} - release date month [x]
    {'col_heading': 'Starting_col_1'} - release date date [x]

     Production [x] - produced by

    Actors: [x] - cast

     Rank - ignored

     Movie - ignored as it is name
     Net gross (Rs) - income_gross
     Verdict - verdict
     Studio - studio
"""

"""
    special conditions
    Movies to ignore:
        wiki_link: 1999_in_film

    Director Name:
        moviename: Aalu Mogalu

    Release date

"""

from django.db.models import Q
from parse2.models import Movie, MovieTableOtherColumnData


def get_main_table_info(each_movie):
    all_table_cols = each_movie.movietableothercolumndata_set.all()
    movie_found_details = {}
    movie_found_details['id'] = each_movie.id
    for each_col in all_table_cols:
        if 'Title' == each_col.col_heading:
            # print("Found Movie Title[{0}]: {1}".format(each_col.col_heading, each_col.get_text()))
            movie_found_details['movie'] = each_col.get_wiki_titles()
        elif 'Director' == each_col.col_heading:
            # print("Found Movie Director[{0}]: {1}".format(each_col.col_heading, each_col.get_text()))
            movie_found_details['director'] = each_col.get_wiki_titles()
        elif 'Cast' == each_col.col_heading or 'Actors' in each_col.col_heading:
            movie_found_details['Cast'] = each_col.get_wiki_titles()
        elif 'Release' == each_col.col_heading or 'Release Date' == each_col.col_heading or 'Release date'.lower() == each_col.col_heading.lower():
            release_date = each_col.get_text().lstrip().rstrip()
            if len(release_date) > 0:
                # print("for Movie[{0}] ReleaseDate[{1}]: {2}".format(each_movie.id, each_col.col_heading, each_col.get_text()))
                movie_found_details['release_date'] = release_date
        elif '_col_0' in each_col.col_heading or 'Starting_col_0' in each_col.col_heading:
            if 'release_date' not in movie_found_details.keys():
                movie_found_details['release_date'] = {}
            movie_found_details['release_date']['month'] = each_col.get_text().replace(' ', '')
            movie_found_details['release_date']['year'] = each_movie.master_list_name.split('_')[-1]
        elif '_col_1' in each_col.col_heading or 'Starting_col_1' in each_col.col_heading:
            if 'release_date' not in movie_found_details.keys():
                movie_found_details['release_date'] = {}
            movie_found_details['release_date']['date'] = each_col.get_text().replace(' ', '')
            movie_found_details['release_date']['year'] = each_movie.master_list_name.split('_')[-1]
        elif 'Genre' in each_col.col_heading:
            movie_found_details['genre'] = each_col.get_wiki_titles()
        elif 'Notes' == each_col.col_heading:
            notes_text = each_col.get_text()
            if len(notes_text):
                # print('Found Notes for movie {0} notes: {1}'.format(each_movie.id, each_col.get_text()))
                movie_found_details['additional_notes'] = notes_text
        elif 'Music' in each_col.col_heading:
            wiki_titles = each_col.get_wiki_titles()
            movie_found_details['music_director'] = []
            for each_title in wiki_titles:
                if len(each_title['text']) > 0:
                    movie_found_details['music_director'].append(each_title)
        elif 'Produce' in each_col.col_heading or 'Production' in each_col.col_heading:
            wiki_titles = each_col.get_wiki_titles()
            movie_found_details['produced_by'] = []
            for each_t in wiki_titles:
                if len(each_t['text']) > 0:
                    if ' by ' in each_t['text']:
                        # print('found by in ',each_t)
                        each_t['text'] = each_t['text'].split(' by ')[-1]
                        # print('by removed ',each_t)
                        movie_found_details['produced_by'].append(each_t)
                    else:
                        movie_found_details['produced_by'].append(each_t)
        elif 'Net gross' in each_col.col_heading:
            movie_found_details['income_gross'] = each_col.get_text()
        elif 'Verdict' in each_col.col_heading:
            movie_found_details['verdict'] = each_col.get_text()
        elif 'Studio' in each_col.col_heading:
            wiki_titles = each_col.get_wiki_titles()
            movie_found_details['studio'] = []
            for each_t in wiki_titles:
                if len(each_t['text']) >0:
                    movie_found_details['studio'].append(each_t)
        elif 'Sources' == each_col.col_heading or 'Ref' == each_col.col_heading \
                or 'Source' in each_col.col_heading \
                or 'INVALID_OR_UNKNOWN' in each_col.col_heading \
                or 'No' in each_col.col_heading \
                or 'Movie' in each_col.col_heading \
                or 'rank' in each_col.col_heading.lower() \
                or 'film' == each_col.col_heading.lower() \
                or 'country' == each_col.col_heading.lower():
            # these cols are ignored so swallowing them
            pass
        else:
            if len(each_col.col_heading) > 0:
                print("Unknown Column Found [{0}] : [{1}]".format(each_col.col_heading, each_col.get_text()), each_movie.id)
    return movie_found_details


def analyse_main_table():
    movies = []
    #all_movies = Movie.objects.filter(Q(wiki_link=None) | Q(wiki_link='') | ~Q(wiki_link__icontains='_in_film'))
    all_movies = Movie.objects.all()
    for each_movie in all_movies:
        movies.append(get_main_table_info(each_movie))
    return movies
