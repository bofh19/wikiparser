__author__ = 'bofh19'

from parse2.models import MovieDetailsFromWikiLink
from bs4 import BeautifulSoup as bs4
from parse2.get_wiki_links_from_html import get_images_text_and_links
from parse2.helper import LOG
import re

# p2 = re.compile("\<[a](?:(?!\/[a]).)+\<\/[a]\>")
ignore_href = ["/wiki/Telugu_language", "/wiki/Tamil_language"]
ignore_href = [x.lower() for x in ignore_href]
ignore_movies = ['Vijaya Kota Veerudu', 'Satya Harishchandra'
    , 'Bhakta Prahlada', 'Savaal', 'Sri Manjunatha', 'Priyamaina Neeku'
    , 'Allare Allari', 'Athili Sattibabu LKG', 'Neninthe', 'Aakasamantha'
                 #tamil
     ,'Bench Talkies - The First Bench'
                 ]

ignore_movies_wiki_links = ['Super_(2010_Indian_film)', 'Ulavacharu_Biryani']


# watch out for the movies 2313 or Vijaya Kota Veerudu
# watch out for movies with weired hypen used to divied Satya Harishchandra or 2628
# watch out for movie Bhakta Prahlada with random text which fails the characters parsing
# watch out for movie Sri Manjunatha, casts are divied by / for multiple languages ?
# watch out for movie Priyamaina Neeku for casts divided by / f
# watch out for movie Allare Allari for casts are just seperated by p tags
# watch out for movie Athili Sattibabu LKG for casts which are just seperated by commas
# watch out for movie Neninthe fails only for last cast where there is an and in the list of casts
# watch out for movie Aakasamantha for multi cast in single line

# ---wikilink ids
# watch out for movie Super_(2010_Indian_film) cas is multiple in single line
# watch out for movie Ulavacharu_Biryani cast is multiple in single line for multi languages
def peek(cast_boxes, i):
    return i, cast_boxes[i]


def is_box_parent_or_child(cast_box):
    soup = bs4(cast_box.tag_raw, 'html.parser')
    if 'cast[edit]' in soup.text.lower():
        return 'p'
    else:
        dls = soup.find_all('dl')
        if len(dls) > 0:
            has_dt = False
            for dl in dls:
                dts = dl.find_all('dt')
                if len(dts) > 0:
                    has_dt = True
            if has_dt:
                return 'p'
        return 'c'


def get_cast_boxes_divided(m):
    cast_boxes = MovieDetailsFromWikiLink.objects.filter(tag_type='cast', movie__id=m.id).order_by('id')
    i = 0
    parsed_boxes = []
    cast_boxes = list(cast_boxes)
    current_object = None
    while i < len(cast_boxes):
        i, peeked_box = peek(cast_boxes, i)
        box_type = is_box_parent_or_child(peeked_box)
        if box_type == 'c':
            if current_object is None:
                current_object = {}
                current_object['c'] = []
                current_object['p'] = None
            current_object['c'].append(peeked_box)
        elif box_type == 'p':
            if current_object is not None:
                parsed_boxes.append(current_object)
            current_object = {}
            current_object['c'] = []
            current_object['p'] = peeked_box
        i = i + 1
    if current_object is not None and (current_object['p'] is not None or len(current_object['c']) > 0):
        parsed_boxes.append(current_object)  # last remaining current object
    return parsed_boxes


def do_movie_specific_replaces(html_data, movie_name):
    txt = bs4(html_data, 'html.parser').get_text()
    if '(Credited as Tom)' in html_data:
        # for movie Padamati Sandhya Ragam
        html_data = html_data.replace('(Credited as Tom)', '(Credited for Tom)')
    if 'Laila in item number' in txt:
        # for movie Nuvve Kavali
        html_data = html_data.replace(' in ', ' as ')
    if '(credited as Aamir Hussain)' in html_data:
        # for movie Holi  - (2002)
        html_data = html_data.replace('(credited as Aamir Hussain)', '(credited for Aamir Hussain)')
    if 'Bhoomika Chawla in a cameo role' in txt:
        # for movie Aadanthe Ado Type
        html_data = html_data.replace(' in a ', ' as ')
    if 'Abhinayasri in item number' in txt:
        # for movie Arya
        html_data = html_data.replace(' in ', ' as ')
    if 'Ramya Krishna in item number' in txt:
        # for movie Adavi Ramudu
        html_data = html_data.replace(' in ', ' as ')
    if 'Aarthi Agarwal in cameo appearance item number' in txt:
        # for movie Chatrapathi
        html_data = html_data.replace(' in ', ' as ')
    if 'Mumaith Khan in an item number' in txt:
        # for movie Pokiri
        html_data = html_data.replace(' in an ', ' as ')

    if 'in song' in txt.lower() or 'in the song' in txt.lower():
        html_data = html_data.replace('in song', 'for song')
        html_data = html_data.replace('in the song', 'for song')

    if ' as ' not in txt and 'item number' in txt.lower():
        html_data = html_data.replace(' in ', ' as ', 1)

    if 'as a bride at a wedding' in txt:
        # for movie Neevalle Neevalle
        html_data = html_data.replace('as a bride at a wedding', 'the a bride at a wedding')
    if 'with Jhansi joining them as' in txt:
        # for movie Neevalle Neevalle
        html_data = html_data.replace('with Jhansi joining them as', 'with Jhansi joining them to')

    if movie_name == 'Magadheera' or movie_name == 'Simha' or movie_name == 'Race Gurram':
        html_data = html_data.replace(' as ', ' ||| ', 1)
        html_data = html_data.replace(' as ', ' a_s ')
        html_data = html_data.replace(' ||| ', ' as ')

        if 'chiranjeevi' in txt.lower():
            html_data = html_data.replace(' in a ', ' as ')

    if movie_name == 'Sahasam':
        if """hired by Sreenidhi's father as her protection""" in txt:
            html_data = html_data.replace(' as her protection', ' for her protection')

    return html_data


def get_data_from_stuff(html_data, movie_name):
    html_data = html_data.replace('<small>as</small>', ' as ')
    soup = bs4(html_data, 'html.parser')
    text = soup.get_text()
    LOG(text)
    if ' - ' in text and ' as ' in text:
        # if both - and as are in the text
        # then user - as the dividing factor for name and char
        # so remove the existing as to nothing and put ' - ' as ' as '
        html_data = html_data.replace(' as ', ' ')

    if ' ... ' in text and ' as ' in text:
        # if both ... and as are in the text, we replace the ... to nothing
        # as use the existing 'as' to divide
        html_data = html_data.replace(' ... ', ' ')

    if ' .... ' in text and ' as ' in text:
        html_data = html_data.replace(' as ', ' for ')
    html_data = html_data.replace(" - ", ' as ')
    html_data = html_data.replace(" ... ", ' as ')
    html_data = html_data.replace(' .... ', ' as ')
    html_data = do_movie_specific_replaces(html_data, movie_name)

    soup = bs4(html_data, 'html.parser')
    ps = soup.find_all('p')
    lis = soup.find_all('li')

    out_obj = {}
    out_obj['names'] = []
    out_obj['other'] = None
    out_obj['chars'] = []

    pt = re.compile("\(.+\)")
    if len(ps) <= 0 and len(lis) <= 0:
        raise Exception("not li or p wtf? or len > 1 len.ps" + str(len(ps)) + " len.lis " + str(len(lis)))

    html_datas = html_data.split(' as ')
    if len(html_datas) > 2:
        html_datas_2 = html_datas[0:2]
        oth = ' as '.join([x for x in html_datas[2:]])
        html_datas_2[1] = html_datas_2[1] + ' ' + oth
        html_datas = html_datas_2
    if len(html_datas) == 1:
        out_obj['names'] = [{'text': '', 'href': ''}]
        contains_bracket = pt.search(html_data)
        if contains_bracket is None:
            text_href = get_link_href_simple(html_data)
            out_obj['names'] = [{'text': text_href['text'], 'href': text_href['href']}]
        else:
            bracket_text = contains_bracket.group()
            html_data = html_data.replace(bracket_text, '')
            bracket = bs4(bracket_text, 'html.parser')
            hrefs = bracket.find_all('a')
            other_text = bracket.get_text()
            out_obj['other'] = {'text': other_text, 'href': []}
            for href in hrefs:
                out_obj['other']['href'].append(href['href'].split('/')[-1])

            text_href = get_link_href_simple(html_data)
            out_obj['names'] = [{'text': text_href['text'], 'href': text_href['href']}]

    elif len(html_datas) == 2:
        name_html = html_datas[0]
        char_html = html_datas[1]

        name_html = name_html.replace(' and ', ' / ')
        name_html_split = name_html.split(' / ')
        for ec_name_html in name_html_split:
            text_href = get_link_href_simple(str(ec_name_html))
            out_obj['names'].append({'text': text_href['text'], 'href': text_href['href']})

        char_html = char_html.replace(' or ', ' / ')
        char_html = char_html.replace(' and ', ' / ')
        char_html_split = char_html.split(' / ')
        for ec_char_split in char_html_split:
            text_hrefs = get_link_href_multiple(str(ec_char_split))
            for text_href in text_hrefs:
                out_obj['chars'].append({'text': text_href['text'], 'href': text_href['href']})
    else:
        LOG(html_data)
        raise Exception("texts length is 0 or > 2 len.texts:" + str(len(html_datas)))
    return out_obj


from parse2.table_helper import *


def get_link_href_multiple(html):
    soup = bs4(html, 'html.parser')
    hrefs_original = soup.find_all('a')
    hrefs = []
    for each_href in hrefs_original:
        if each_href['href'].lower() in ignore_href:
            pass
        elif each_href['href'][0] == '#':
            pass
        else:
            hrefs.append(each_href)
    if len(hrefs) > 1:
        outs = []
        for href in hrefs:
            outs.append(get_link_href_simple(str(href)))
        return outs
    else:
        return [get_link_href_simple(html)]


def get_link_href_simple(html):
    try:
        soup = bs4(html, 'html.parser')
        out = {}
        out['text'] = soup.get_text()
        out['href'] = ''
        hrefs_original = soup.find_all('a')
        hrefs = []
        for each_href in hrefs_original:
            LOG(each_href['href'])
            if each_href['href'].lower() in ignore_href:
                pass
            elif each_href['href'][0] == '#' or each_href['href'] in ['/wiki/Guest_appearance',
                                                                      '/wiki/Cameo_appearance'
                                                                      ] \
                    or 'Vasco Da Gama'.lower() in each_href.get_text().lower() or 'Estêvão da Gama'.lower() in each_href.get_text().lower():
                pass
            else:
                hrefs.append(each_href)
        if len(hrefs) > 1:
            #raise Exception("not simple more than 1 href " + html)
            # ignoreing and picking up one href
            hrefs = hrefs[0:1]
        if len(hrefs) == 1:
            out['href'] = hrefs[0]['href'].split('/')[-1]
    except Exception as e:
        LOG("ERROR")
        LOG(html)
        raise e
    return out


def get_casts_from_table(html):
    casts = []
    soup = bs4(html, 'html.parser')
    ths = get_table_as_list(soup, True)
    if len(ths) <= 0:
        raise Exception("no header info found")
    LOG("header info found: " + str(ths))
    th = ths[0]
    char_row = None
    name_row = None
    for row_num, each_th in enumerate(th):
        each_th = bs4(each_th, 'html.parser')
        txt = each_th.get_text().rstrip().lstrip()
        LOG('row_num: {0} is {1}'.format(row_num, txt))
        if txt.lower() in ['character', 'role', 'characters', 'Character Name'.lower()]:
            char_row = row_num
        elif 'actor' in txt.lower():
            name_row = row_num
        elif 'actress' in txt.lower():
            name_row = row_num
        elif txt.lower() in ['Name'.lower()]:
            name_row = row_num

    if char_row is None or name_row is None:
        raise Exception("unable to find character row or name row")

    for tr in get_table_as_list(soup):
        all_names = []
        all_chars = []
        LOG(str(tr))
        actor = tr[name_row]
        if char_row < len(tr):
            char = tr[char_row]
        else:
            char = ''

        names_split = actor.split('<br/>')
        for ec_name in names_split:
            text_href = get_link_href_simple(str(ec_name))
            all_names.append({'text': text_href['text'], 'href': text_href['href']})

        char = char.replace(' / ', ' or ')
        char = char.replace(' /', ' or ')
        chars_split = char.split(' or ')
        for ec_split in chars_split:
            text_href = get_link_href_simple(str(ec_split))
            all_chars.append({'text': text_href['text'], 'href': text_href['href']})

        out_obj = {}
        out_obj['names'] = all_names
        out_obj['chars'] = all_chars

        all_html = ''
        for ex in tr:
            all_html += ex
        out_obj['all_text'] = bs4(all_html, 'html.parser').get_text()
        out_obj['other'] = {'text': '', 'href': []}
        casts.append(out_obj)
    return casts


def get_all_lis_recursive(li):
    current_lis = []
    lis = li.find_all('li')
    if len(lis) > 0:
        for li_internal in lis:
            current_lis.extend(get_all_lis_recursive(li_internal))
        return current_lis
    else:
        return [li]


def get_cast_for_movie(m):
    LOG("-----------------START FOR CASTS-------------------------")
    LOG('movie: ' + str(m.id) + ' Name: ' + m.movie_name)
    if m.movie_name in ignore_movies or m.wiki_link in ignore_movies_wiki_links:
        LOG("Movie ignored as in ignore list return empty list")
        return []
    output_casts = []
    casts = get_cast_boxes_divided(m)
    for cast in casts:
        cur_cast = {}
        cur_cast['type'] = {}
        cur_cast['data'] = {}
        cur_cast['images'] = None
        cur_cast['additional'] = []
        parent = cast['p']
        childs_original = cast['c']
        childs = []
        for each_child in childs_original:
            tmp_c_soup = bs4(each_child.tag_raw, 'html.parser')
            imgs = tmp_c_soup.find_all('img')
            if len(imgs) >= 1:
                cur_cast['images'] = get_images_text_and_links(str(each_child.tag_raw))
            else:
                txt = tmp_c_soup.get_text()
                if 'Additionally' in txt:
                    cur_cast['additional'].append({'html': str(each_child.tag_raw)})
                else:
                    childs.append(each_child)

        if parent is not None:
            soup = bs4(parent.tag_raw, 'html.parser')
            # cur_cast['type']['raw'] = parent.tag_raw
            cur_cast['type']['text'] = soup.get_text()
        else:
            cur_cast['type'] = None
        all_html = ''
        for each_child in childs:
            all_html += str(each_child.tag_raw)
        # cur_cast['data']['raw'] = all_html
        cur_cast['data']['casts'] = []
        if len(all_html) > 0:
            soup = bs4(all_html, 'html.parser')
            tables = soup.find_all('table')
            if len(tables) > 0:
                LOG("found tables: " + str(len(tables)))
                for each_table in tables:
                    cur_cast['data']['casts'].extend(get_casts_from_table(str(each_table)))
            else:
                ps = soup.find_all('p')
                if len(ps) > 0:
                    LOG('length of ps > 0: ' + str(len(ps)))
                    for each_p in ps:
                        each_p_text = each_p.get_text()
                        if len(each_p_text) > 1:
                            text_links = get_data_from_stuff(str(each_p), m.movie_name)
                            if text_links is not None:
                                cur_cast['data']['casts'].append(text_links)
                lis = soup.find_all('li')
                LOG(str(lis))
                if len(lis) > 0:
                    lis_final = []
                    for li in lis:
                        lis_final.extend(get_all_lis_recursive(li))
                    lis = lis_final
                    LOG('length of li > 0 : ' + str(len(lis)))
                    for each_li in lis:
                        # LOG('EACH_LI: ' + str(each_li))
                        text_links = get_data_from_stuff(str(each_li), m.movie_name)
                        if text_links is not None:
                            cur_cast['data']['casts'].append(text_links)
                if len(lis) <= 0 and len(ps) <= 0:
                    cur_cast['data']['unknown'] = str(all_html)
                    raise Exception("not li or p or table")
        output_casts.append(cur_cast)
    LOG('OUTPUT CASTS ' + str(output_casts))
    return output_casts
