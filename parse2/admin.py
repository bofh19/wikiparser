# -*- coding: utf-8 -*-
import json

__author__ = 'skarumuru'

from parse2.models import *
from django.contrib import admin
from django import forms
from django.utils.html import format_html
from urllib.request import urlopen


class MovieForm(forms.ModelForm):
    class Meta:
        model = Movie
        fields = '__all__'


class MovieDetailsFromWikiLinkInline(admin.TabularInline):
    model = MovieDetailsFromWikiLink
    extra = 0
    readonly_fields = (
        'id', 'tag_type', 'parent_id', 'html_view', 'tag_raw', 'movie', 'other_temp_id', 'parent_details')

    def id(self, instance):
        return instance.id

    def html_view(self, instance):
        try:
            return format_html(instance.tag_raw)
        except Exception as e:
            return str(e)

    html_view.short_description = "html view"

    def movie(self, instance):
        return Movie.objects.get(id=instance.movie_id).movie_name

    def parent_id(self, instance):
        if instance.parent_details:
            # return MovieDetailsFromWikiLink.objects.get(id=instance.parent_details_id).id
            return instance.parent_details_id
        else:
            return ''

    movie.short_description = "Movie"
    parent_id.short_description = "Parent"


class MovieTableOtherColumnDataInline(admin.TabularInline):
    model = MovieTableOtherColumnData
    extra = 0

    readonly_fields = ('col_data_text', 'col_heading', 'col_data', 'movie')

    def movie(self, instance):
        return Movie.objects.get(id=instance.movie_id).movie_name

    def col_data_text(self, instance):
        wiki_links = instance.get_wiki_titles()
        html = '<ul>'
        for ew in wiki_links:
            if len(ew['text']) > 0:
                html += '<li> {0}  [{1}] </li>'.format(ew['text'], ew['href'])
        return format_html(html)

    movie.short_description = "Movie"
    col_data_text.short_description = "col data readonly"


from django.contrib.admin import SimpleListFilter
from django.db.models import Q


class MovieMasterListFilter(SimpleListFilter):
    title = 'Master List'
    parameter_name = 'master_list_name'

    default_value = None

    def lookups(self, request, model_admin):
        list_of_main_lists = []
        queryset = Movie.objects.order_by('-master_list_name')
        if 'language' in request.GET:
            queryset = queryset.filter(language=request.GET['language'])
        queryset = queryset.values('master_list_name').distinct()
        print('----------', len(queryset))
        for itm in queryset:
            list_of_main_lists.append(
                (itm['master_list_name'], itm['master_list_name'].replace('List_of_', '').replace('_', ' ')))
        return list_of_main_lists

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(master_list_name=self.value())
        return queryset


class MovieWikiLinkFilter(SimpleListFilter):
    title = "Wiki Link"
    parameter_name = "wiki_link__isnull"

    default_value = None

    def lookups(self, request, model_admin):
        return [("True", "Is Null"), ("False", "Is Not Null")]

    def queryset(self, request, queryset):
        if self.value():
            val = self.value().lstrip().rstrip().lower()
            if val == "true":
                return queryset.filter(~Q(wiki_link__isnull=False))
            else:
                return queryset.filter(wiki_link__isnull=False)
        return queryset


class MovieAdmin(admin.ModelAdmin):
    form = MovieForm
    search_fields = ['movie_name', 'wiki_link']
    list_filter = ['language', 'wiki_link_error', MovieWikiLinkFilter, MovieMasterListFilter]
    list_display = ['movie_name', 'id', 'wiki_link', 'master_list_name']
    readonly_fields = ('next', 'custom_save', 'possible_wikipedia')

    inlines = [
        MovieDetailsFromWikiLinkInline,
        MovieTableOtherColumnDataInline
    ]

    def next(self, instance):
        next_id = int(instance.id) + 1
        return format_html('<a href="../../{0}/">next</a>'.format(next_id))

    def possible_wikipedia(self, instance):
        try:
            if instance.wiki_link is None or len(instance.wiki_link) <= 0:
                url = "https://en.wikipedia.org/w/api.php?action=opensearch&search={0}".format(instance.movie_name)
                f = urlopen(url)
                data = f.read().decode('utf8')
                json_data = json.loads(data)
                descs = json_data[1]
                sh_desc = json_data[2]
                links = json_data[3]
                html = '<ul>'
                for which, each_descs in enumerate(descs):
                    html += '<li> <p> <b>{0}</b> - {1}  [ {2} ] </p></li>'.format(each_descs, links[which],
                                                                                  sh_desc[which])
                html += '</ul>'
                return format_html(html)
            else:
                return 'wiki link already existing'
        except Exception as e:
            return str(e)

    def custom_save(self, instance):
        return format_html(
            '<script type="text/javascript"> function click_on_name_continue() ' +
            '{{ document.getElementsByName("_continue")[0].click() }}</script>' +
            '<a href="#" onclick="click_on_name_continue();">save</a>')

    next.short_description = "next movie"
    custom_save.short_description = "Save"
    possible_wikipedia.short_description = "Possible wiki pedia links"


admin.site.register(Movie, MovieAdmin)


### wiki link data backup

class InlineHtmlWikiLinkData(admin.ModelAdmin):
    model = WikiPediaLinkData
    readonly_fields = ('data_html_view',)

    def data_html_view(self, instance):
        return format_html('<iframe width="100%" style="min-height: 1000px" src="/api/WikiPediaLinkDataRaw/' + str(
            int(instance.id)) + '/"></iframe>')

    data_html_view.short_description = "html view"


admin.site.register(WikiPediaLinkData, InlineHtmlWikiLinkData)
