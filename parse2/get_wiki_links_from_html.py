__author__ = 'skarumuru'

from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse

def get_text_and_links(html_data):
    out = []
    try:
        x = []
        html_data = html_data.replace('<br/>', ',').replace('<br>', ',')
        soup = bs(html_data, "html.parser")

        initial_items = [x.rstrip().lstrip() for x in soup.get_text().split(',')]
        for ea in soup.find_all('a'):
            try:
                if "index.php" not in ea['href']:
                    x.append(ea)
            except Exception as e:
                pass
        if len(x) <= 0:
            out = []
            for x in initial_items:
                out.append({'href': '', 'text': x})  # no hrefs found so just return text
            return out

        for ea in x:  # found multiple hrefs return those.
            s = ea['href'].split('/')
            text = ea.get_text().lstrip().rstrip()
            if len(s) > 0 and 'index.php' not in s[len(s) - 1] and '#' != s[-1][0]:
                if text in initial_items:
                    initial_items.remove(text)
                out.append({'href': s[len(s) - 1], 'text': text})
            else:
                try:
                    parsed = urlparse.parse_qs(urlparse.urlparse(ea).query)
                    if text in initial_items:
                        initial_items.remove(text)
                    out.append({'href': parsed['href'], 'text': text})
                except Exception as e:
                    pass
        for each_left in initial_items:
            out.append({'href': '', 'text': each_left})
        return out
    except Exception as e:
        return out

def get_images_text_and_links(html_data):
    out = {}
    out['images'] = []
    try:
        soup = bs(html_data, 'html.parser')
        imgs = soup.find_all('img')
        for img in imgs:
            tmp = img['src']
            out['images'].append(tmp)
        out['text'] = soup.text
        return out
    except:
        return out