# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-26 00:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parse2', '0002_movie_master_list_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='language',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
