__author__ = 'bofh'

from parse1.models import MovieListMain
from parse2.models import MovieTableOtherColumnData, Movie
from parse1.identify_movies2 import getMoviesForThisTitle2


def get_all_movies():
    for i in range(3, 10):
        q = "List_of_Telugu_films_of_the_19" + str(i) + "0s"
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)
    for i in range(0, 10):
        q = "List_of_Telugu_films_of_194" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)
    for i in range(0, 10):
        q = "List_of_Telugu_films_of_195" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)
    for i in range(0, 10):
        q = "List_of_Telugu_films_of_196" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)

    for i in range(0, 10):
        q = "List_of_Telugu_films_of_197" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)

    for i in range(0, 10):
        q = "List_of_Telugu_films_of_198" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)

    for i in range(0, 10):
        q = "List_of_Telugu_films_of_199" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)

    for i in range(0, 10):
        q = "List_of_Telugu_films_of_200" + str(i)
        print("GETTING FOR : ", q)
        get_movies_from_this_list(title=q)

    for i in range(0, 6):
        try:
            q = "List_of_Telugu_films_of_201" + str(i)
            print("GETTING FOR : ", q)
            get_movies_from_this_list(title=q)
        except Exception as e:
            print(e)


def get_movies_from_this_list(title="List_of_Telugu_films_of_the_1930s", lang="telugu"):
    getMoviesForThisTitle2(title)
    movie_main_list = MovieListMain.objects.filter(givenWikiTitle=title)
    if len(movie_main_list) != 1:
        print("movie main list length is weired something is wrong ", len(movie_main_list))
        return
    else:
        movie_main_list = movie_main_list[0]
    tables = movie_main_list.table_set.all()
    for table in tables:
        rows = table.row_set.all()
        for which_row, each_row in enumerate(rows):
            movie_cols = each_row.getMovieCol()
            for each_movie_col in movie_cols:
                movie_name = each_movie_col.getText()
                already_existing = Movie.objects.filter(movie_name=movie_name,
                                                        master_list_name=each_row.table_movie_list.table_movie_list.givenWikiTitle)
                if len(already_existing) > 0:
                    movie = already_existing[0]
                    created = False
                    # delete any other duplicates if they exist
                    # stupid condition as we don't have unique on list and movie_name
                    if len(already_existing) > 1:
                        print("More than 2 movies existing deleting others ")
                        for which, x_m in enumerate(already_existing):
                            if which != 0:
                                print("Deleting ", x_m.id)
                                x_m.delete()
                            else:
                                print("Not Deleting", x_m.id)
                else:
                    created = True
                if created:
                    movie = Movie()
                    movie.movie_name = movie_name
                    movie.language = lang
                    movie.master_list_name = each_row.table_movie_list.table_movie_list.givenWikiTitle
                    row_movie_wikilinks = each_movie_col.getWikiPageTitle()
                    if len(row_movie_wikilinks) >= 1:
                        row_movie_wiki_link = row_movie_wikilinks[0]
                        movie.wiki_link = row_movie_wiki_link
                else:
                    print('movies already exist')
                    print('Movie: {0}, id: {1}, wiki_link: {2}'.format(movie.movie_name, movie.id, movie.wiki_link))
                movie.save()
                if movie.movietableothercolumndata_set.all().count() <= 0:
                    other_columns = each_row.columndata_set.all()
                    for each_other_col in other_columns:
                        mtd = MovieTableOtherColumnData()
                        mtd.movie = movie
                        mtd.col_heading = each_other_col.col_heading_id.getText()
                        mtd.col_data = each_other_col.col_raw
                        mtd.save()
