from parse2.models import *

from bs4 import BeautifulSoup as bs4

def get_top_summary_for_movie(m):
    movie = Movie.objects.get(id=m.id)
    top_summaries = movie.moviedetailsfromwikilink_set.filter(tag_type='top_summary').order_by('id')
    out_tags = []
    for each_sum in top_summaries:
        soup = bs4(each_sum.tag_raw, 'html.parser')
        hrefs = soup.find_all('a')
        for href in hrefs:
            link = href['href']
            if len(link) > 0 and '#cite' in link:
                href.extract()
        txt = soup.get_text()
        if len(txt) > 0:
            out_tags.append(txt)
    return out_tags

def get_plot_for_movie(m):
    movie = Movie.objects.get(id=m.id)
    plots = movie.moviedetailsfromwikilink_set.filter(tag_type='plot').order_by('id')
    out_tags = []
    for plot in plots:
        soup = bs4(plot.tag_raw, 'html.parser')
        hrefs = soup.find_all('a')
        for href in hrefs:
            link = href['href']
            if len(link) > 0 and '#cite' in link:
                href.extract()
        txt = soup.get_text()
        if len(txt) > 0 and txt.lower() != 'PLOT[EDIT]'.lower():
            out_tags.append(txt)
    return out_tags