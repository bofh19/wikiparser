from parse1.models import Row, ColumnData, ColumnHeading, WikiLink
from parse1.models import WikiLinkDataDivided, WikiLinkData, LinkToWikiLinkData

from parse2.models import *

"""
    this method finds out all movies list
    and saves them into their tables MovieDetailsFromWikiLink
    and saves the column data from lists into MovieTableOtherColumnData
"""


def find_all_movies():
    global movies_found
    rows = Row.objects.all()  # each row has movie

    for which_row,each_row in enumerate(rows):
        movie_cols = each_row.getMovieCol()
        for each_movie_col in movie_cols:
            movie_name = each_movie_col.getText()
            movie = Movie()
            movie.movie_name = movie_name
            movie.master_list_name = each_row.table_movie_list.table_movie_list.givenWikiTitle
            movie.save()
            row_movie_wikilinks = each_movie_col.getWikiPageTitle()
            print('saving movie',movie_name,'row_id',each_row.id,'count:',which_row,'MovieId: ',movie.id)
            if len(row_movie_wikilinks) >= 1:
                row_movie_wiki_link = row_movie_wikilinks[0]
                wiki_links = WikiLink.objects.filter(link_title=row_movie_wiki_link)
                wiki_link = wiki_links[0]
                link_to_data = LinkToWikiLinkData.objects.get(wiki_link=wiki_link)
                if link_to_data.is_title_redirect:
                    wiki_link = link_to_data.is_title_rected_to
                # check if this movie is already in db
                if Movie.objects.filter(wiki_link=wiki_link.link_title).exists():
                    print("skipping movie ",movie_name, movie.id, " duplicate found")
                    movie.delete()  # delete this duplicate entry and
                    continue  # skip everything else as it was already done for previous entry

                movie.wiki_link = wiki_link.link_title
                movie.save()

                wiki_link_data = wiki_link.wikilinkdatadivided_set.all()
                for w_data in wiki_link_data:
                    movie_details = MovieDetailsFromWikiLink()

                    movie_details.other_temp_id = w_data.id
                    movie_details.movie = movie
                    movie_details.tag_type = w_data.type
                    movie_details.tag_raw = w_data.tag_data
                    if w_data.parent:
                        tmp_parent = MovieDetailsFromWikiLink.objects.get(other_temp_id=w_data.parent_id)
                        movie_details.parent_details = tmp_parent
                    movie_details.save()

            else:
                pass

            other_columns = each_row.columndata_set.all()
            for each_other_col in other_columns:
                mtd = MovieTableOtherColumnData()
                mtd.movie = movie
                mtd.col_heading = each_other_col.col_heading_id.getText()
                mtd.col_data = each_other_col.col_raw
                mtd.save()
                # print(each_row.id, len(row_movie_wikilinks) , '<=0')
                # wiki_links = WikiLink.objects.filter(link_title=each_movie_col.getWikiPageTitle())
                #
                #
                # if len(wiki_links) >= 1:
                #     movies_found['gt_1'].append(each_row.id)
                # elif len(wiki_links) <= 0:
                #     movies_found['lt_1'].append(each_row.id)

from parse1.get_link_data import get_data_from_url
# todo - check for movies that have wikilink but no wikilink data divided
# get those pages save them and parse them
def find_movies_with_link_and_no_data():
    movies = Movie.objects.all()
    count = 0
    for em in movies:
        if em.wiki_link and len(em.wiki_link) > 0:
            mds = em.moviedetailsfromwikilink_set.all()
            if len(mds) <= 0:
                print(em, em.id, em.wiki_link)
                count += 1

