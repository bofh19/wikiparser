from bs4 import BeautifulSoup
from bs4 import UnicodeDammit
from django.db import models

from parse1.parse_page_data import parse_data
from parse2.get_link_data import get_link_data


class WikiPediaLinkData(models.Model):
    wiki_link = models.CharField(max_length=255, unique=True)
    data_raw = models.TextField(blank=True, null=True)
    wiki_link_redirects = models.CharField(max_length=1024, blank=True, null=True)
    wiki_link_error = models.BooleanField(default=False)

    def __str__(self):
        return self.wiki_link


class Movie(models.Model):
    def __init__(self, *args, **kwargs):
        super(Movie, self).__init__(*args, **kwargs)
        self.old_wiki_link = self.wiki_link

    movie_name = models.CharField(max_length=512)
    wiki_link = models.CharField(max_length=512, blank=True, null=True)
    master_list_name = models.CharField(max_length=512, blank=True, null=True)
    language = models.CharField(max_length=255, blank=True, null=True)
    wiki_link_redirects = models.CharField(max_length=1024, blank=True, null=True)
    wiki_link_error = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        print('[DATABASE]', 'save')
        super(Movie, self).save(*args, **kwargs)
        all_related = self.moviedetailsfromwikilink_set.all()
        print('[DATABASE]',
              'Length of related {0}, old link {1}, new link {2}'.format(len(all_related), self.old_wiki_link,
                                                                         self.wiki_link))
        print('[DATABASE]', 'Movie: ', self.id, ' name: ', self.movie_name, ' lang: ', self.language)
        if self.wiki_link is None or len(self.wiki_link) <= 0:
            # if wiki link is made null or empty delete all related wiki link data for this movie
            self.moviedetailsfromwikilink_set.all().delete()
        elif self.wiki_link != self.old_wiki_link or len(all_related) <= 0:
            # this is here because wiki link is not null and length of of wiki link is > 0
            # or if length of related wiki data is nothing
            # so create this new link and use it
            # new wiki link create it
            print('[DATABASE]', 'calling fetch parse and save')

            error, redirect, redirect_data, data = get_link_data(self.wiki_link)
            #### backup wiki link data
            wiki_data_backup, wiki_data_backup_created = WikiPediaLinkData.objects.get_or_create(
                wiki_link=self.wiki_link)
            if wiki_data_backup_created:
                wiki_data_backup.save()
            if error:
                wiki_data_backup.wiki_link_error = True
                wiki_data_backup.save()
            else:
                if redirect:
                    wiki_data_backup.wiki_link_redirects = redirect_data
                wiki_data_backup.data_raw = data
            try:
                wiki_data_backup.save()
            except Exception as e:
                pass
            #### backup wiki link data ends

            if error:
                # if error'ed out then skip the record without editing the wiki link
                self.wiki_link_error = True
                self.old_wiki_link = self.wiki_link  # to avoid recursive stuck, make both old and new links equal
                super(Movie, self).save(*args, **kwargs)  # call save
                return  # exit
            if redirect:
                # if redirect save it to db
                self.old_wiki_link = self.wiki_link  # make both same to avoid self super recursive call fuk
                self.wiki_link_redirects = redirect_data
                super(Movie, self).save(*args, **kwargs)
                # continue as we anyway get data

            title_data = data

            # if we got the data then
            # delete all old data, parse it and put the new data
            # else put back the old wiki link
            if title_data:
                data = title_data
                nodes, childs = parse_data(data)

                self.moviedetailsfromwikilink_set.all().delete()

                for node in nodes:
                    movie_details = MovieDetailsFromWikiLink()
                    movie_details.movie = self
                    movie_details.tag_type = node.type
                    movie_details.tag_raw = str(node.tag)
                    try:
                        movie_details.save()
                        for each_child_item in node.child_tags:
                            movie_details_child = MovieDetailsFromWikiLink()
                            movie_details_child.movie = self
                            movie_details_child.tag_type = node.type
                            movie_details_child.tag_raw = str(each_child_item)
                            movie_details_child.parent_details = movie_details
                            movie_details_child.save()
                    except Exception as e:
                        pass
            else:
                # failed to get data
                # so put back the old data
                self.wiki_link = self.old_wiki_link
                super(Movie, self).save(*args, **kwargs)

    def __str__(self):
        return self.movie_name


class MovieDetailsFromWikiLink(models.Model):
    movie = models.ForeignKey(Movie)
    tag_type = models.CharField(max_length=512)
    parent_details = models.ForeignKey('MovieDetailsFromWikiLink', null=True, blank=True)
    tag_raw = models.TextField()
    other_temp_id = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.tag_type


from urllib import parse as urlparse


class MovieTableOtherColumnData(models.Model):
    movie = models.ForeignKey(Movie)
    col_heading = models.CharField(max_length=512)
    col_data = models.TextField()

    def get_text(self):
        """
        returns text data from col_raw
        """
        try:
            soup = BeautifulSoup(self.col_data, "html.parser")
            return soup.get_text().replace("\n", " ")
        except Exception as e:
            return self.col_raw

    def get_wiki_titles(self):
        out = []
        try:
            x = []
            soup = BeautifulSoup(self.col_data, "html.parser")
            initial_items = [x.rstrip().lstrip() for x in soup.get_text().split(',')]
            for ea in soup.find_all('a'):
                try:
                    if "index.php" not in ea['href']:
                        x.append(ea)
                except Exception as e:
                    pass
            if len(x) <= 0:
                out = []
                for x in initial_items:
                    out.append({'href': '', 'text': x})  # no hrefs found so just return text
                return out

            for ea in x:  # found multiple hrefs return those.
                s = ea['href'].split('/')
                text = ea.get_text().lstrip().rstrip()
                if len(s) > 0 and 'index.php' not in s[len(s) - 1] and '#' != s[-1][0]:
                    if text in initial_items:
                        initial_items.remove(text)
                    out.append({'href': s[len(s) - 1], 'text': text})
                else:
                    try:
                        parsed = urlparse.parse_qs(urlparse.urlparse(ea).query)
                        if text in initial_items:
                            initial_items.remove(text)
                        out.append({'href': parsed['href'], 'text': text})
                    except Exception as e:
                        pass
            for each_left in initial_items:
                out.append({'href': '', 'text': each_left})
            return out
        except Exception as e:
            return out
        return out
