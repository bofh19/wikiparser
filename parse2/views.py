from django.shortcuts import render_to_response
from django.template import RequestContext

from parse2.models import *


def get_wiki_link_data_raw(request, data_id):
    wiki_link_data_obj = WikiPediaLinkData.objects.get(id=data_id)
    return render_to_response('parse2/wiki_link_raw_view.html', {'ob': wiki_link_data_obj},
                              context_instance=RequestContext(request))