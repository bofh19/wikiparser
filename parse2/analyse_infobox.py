__author__ = 'skarumuru'

from parse2.models import *
from bs4 import BeautifulSoup as bs

"""
    this file has analyses all the infoboxes in movie wikipedia datas
    top left side box
"""

"""
 -- box columns

 ['Also known as',      - alternate_name
 'Audio format',        - audio_format
 'Awards',              - awards
 'Based on',            - based_on
 'Box office',          - income_gross
 'Budget',              - budget
 'Camera setup',        - camera_setup
 'Characters',          - characters
 'Cinematography',      - cinematography
 'Composer(s)',         - composers
 'Country',             - country
 'Country of origin',   - country_origin
 'Created by',          - created_by
 'Date premiered',      - date_premiered
 'Directed by',         - directors
 'Distributed by',      - distributors
 'Distributor',         - distributors
 'Edited by',           - editors
 'Editor(s)',           - editors
 'Executive producer(s)',- executive_producers
 'First shown in',      - first_shown_in
 'Genre',               - genre
 'Label',               - label
 'Language',            - language
 'Length',              - length
 'Location',            - locations
 'Location(s)',         - locations
 'Music by',            - music
 'Narrated by',         - narrators
 'No. of episodes',     - no_of_episodes
 'No. of seasons',      - no_of_seasons
 'Opening theme',       - opening_theme
 'Original language',   - original_languages
 'Original language(s)',- original_languages
 'Original network',    - original_network
 'Original release',    - original_release
 'Original title',      - original_title
 'Outcome',             - outcome
 'Picture format',      - picture_format
 'Place premiered',     - place_premiered
 'Preceded by',         - preceded_by
 'Presented by',        - presented_by
 'Produced by',         - producers
 'Producer',            - producers
 'Producer(s)',         - producers
 'Production company(s)',- production_company
 'Productioncompanies', - production_company
 'Productioncompany',   - production_company
 'Recorded',            - recorded
 'Release dates',       - release_dates
 'Released',            - release_dates
 'Running time',        - running_time
 'Screenplay by',       - screenplay
 'Setting',             - setting
 'Starring',            - starring
 'Story by',            - story_by
 'Subject',             - subject
 'Theme music composer',- music_theme
 'Verdict',             - verdict
 'Voices of',           - voices
 'Written by']          - writers

"""

"""
    # exceptions:
        Money Money -> wrong wiki link -> wrong data
"""


def get_wiki_titles(html_data):
    out = []
    try:
        x = []
        html_data = html_data.replace('<br/>', ',').replace('<br>', ',')
        soup = BeautifulSoup(html_data, "html.parser")

        initial_items = [x.rstrip().lstrip() for x in soup.get_text().split(',')]
        for ea in soup.find_all('a'):
            try:
                if "index.php" not in ea['href']:
                    x.append(ea)
            except Exception as e:
                pass
        if len(x) <= 0:
            out = []
            for x in initial_items:
                out.append({'href': '', 'text': x})  # no hrefs found so just return text
            return out

        for ea in x:  # found multiple hrefs return those.
            s = ea['href'].split('/')
            text = ea.get_text().lstrip().rstrip()
            if len(s) > 0 and 'index.php' not in s[len(s) - 1] and '#' != s[-1][0]:
                if text in initial_items:
                    initial_items.remove(text)
                out.append({'href': s[len(s) - 1], 'text': text})
            else:
                try:
                    parsed = urlparse.parse_qs(urlparse.urlparse(ea).query)
                    if text in initial_items:
                        initial_items.remove(text)
                    out.append({'href': parsed['href'], 'text': text})
                except Exception as e:
                    pass
        for each_left in initial_items:
            out.append({'href': '', 'text': each_left})
        return out
    except Exception as e:
        return out


def get_box_table_cols(box):
    soup = bs(box.tag_raw, 'html.parser')
    tables = soup.find_all('table')
    if len(tables) != 1:
        print("Found more or less tables in ", box.movie.id, box.movie.movie_name, len(tables))
        return []
    else:
        table = tables[0]
        rows = []
        for each_row in table.find_all('tr'):
            row = {}
            row_th = []
            row_td = []
            for each_row_heading in each_row.find_all('th'):
                row_th.append({'html': str(each_row_heading), 'text': each_row_heading.get_text().lstrip().rstrip()})
            for each_row_tds in each_row.find_all('td'):
                row_td.append({'html': str(each_row_tds), 'text': each_row_tds.get_text().lstrip().rstrip()})
            row['th'] = row_th
            row['td'] = row_td
            rows.append(row)
        return rows


def get_this_row_formatted(row):
    row_out = {}
    if len(row['th']) == 1 and len(row['td']) == 1:
        row_out['heading'] = get_wiki_titles(row['th'][0]['html'])
        row_out['values'] = get_wiki_titles(row['td'][0]['html'])
    else:
        if len(row['th']) == 1 and (len(row['td']) <= 0 or len(row['td'][0]['text']) <= 0):
            row_out['heading'] = [{'text':'box_title', 'href': ''}]
            row_out['values'] = [{'text': row['th'][0]['text'], 'href':''}]
        elif (len(row['th']) == 0 or len(row['th'][0]['text']) <= 0) and len(row['td']) == 1:
            soup = bs(row['td'][0]['html'], 'html.parser')
            imgs = soup.find_all('img')
            if len(imgs) >= 0:
                images = []
                for img in imgs:
                    images.append(img['src'])
                row_out['heading'] = [{'text':'box_image', 'href': ''}]
                row_out['values'] = []
                for img in images:
                    row_out['values'].append( {'text':img, 'href': img} )
                # should perform stuff more here
    return row_out

def get_movie_info_for_movie(m):
    movie_summary_boxes = MovieDetailsFromWikiLink.objects.filter(tag_type='top_summary_box', movie__id = m.id )
    all_boxes_info = []
    for each_box in movie_summary_boxes:
        box_table_info = {}
        box_table_info['movie'] = {'name': each_box.movie.movie_name, 'id': each_box.movie.id}
        box_table_info['rows'] = get_box_table_cols(each_box)
        all_boxes_info.append(box_table_info)
    movie_infos = []
    for eb in all_boxes_info:
        movie_info = {}
        movie_info['movie'] = eb['movie']
        movie_info['details'] = []
        movie_info['no_info_rows'] = []
        for eb_row in eb['rows']:
            details = get_this_row_formatted(eb_row)
            if len(details.keys()) <= 0:
                movie_info['no_info_rows'].append(eb_row)
            else:
                movie_info['details'].append(details)
        movie_infos.append(movie_info)
    return movie_infos

def get_data_from_info_box():
    all_boxes = MovieDetailsFromWikiLink.objects.filter(tag_type='top_summary_box')
    all_boxes_info = []
    for each_box in all_boxes:
        box_table_info = {}
        box_table_info['movie'] = {'name': each_box.movie.movie_name, 'id': each_box.movie.id}
        box_table_info['rows'] = get_box_table_cols(each_box)
        all_boxes_info.append(box_table_info)
    unique_col_ths = []
    for ebi in all_boxes_info:
        print('Movie: ', ebi['movie'])
        for ebi_row in ebi['rows']:
            if (len(ebi_row['td']) <= 0) or (len(ebi_row['td']) == 1 and len(ebi_row['td'][0]['text']) == 0):
                continue
            else:
                if len(ebi_row['th']) == 1 and len(ebi_row['th'][0]['text']) > 0:
                    unique_col_ths.append(ebi_row['th'][0]['text'])
    unique_col_ths = list(set(unique_col_ths))
    movie_infos = []
    for eb in all_boxes_info:
        movie_info = {}
        movie_info['movie'] = eb['movie']
        movie_info['details'] = []
        movie_info['no_info_rows'] = []
        for eb_row in eb['rows']:
            details = get_this_row_formatted(eb_row)
            if len(details.keys()) <= 0:
                movie_info['no_info_rows'].append(eb_row)
            else:
                movie_info['details'].append(details)
        movie_infos.append(movie_info)
    return movie_infos
    #a = [x for x in movie_infos if 'box_image' in [z[0]['text'] for z in [y['heading'] for y in x['details']]] ]