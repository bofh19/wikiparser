import inspect

allow_log = True


def LOG(s, lg_level=1):
    if allow_log:
        fn = inspect.stack()[1][3]
        if lg_level == 1:
            print('[' + fn + ']: ' + s)
        if lg_level == 2:
            print('ERROR[' + fn + ']: ' + s)
