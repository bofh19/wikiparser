from bs4 import BeautifulSoup as bs4
from bs4 import UnicodeDammit as ud4

import inspect

allow_log = True


# this one parses the side table data

def LOG(s, lg_level=1):
    if allow_log:
        fn = inspect.stack()[1][3]
        if lg_level == 1:
            print('[' + fn + ']: ' + s)
        if lg_level == 2:
            print('ERROR[' + fn + ']: ' + s)


def get_children_count(tag):
    if tag is None:
        return 0
    if not hasattr(tag, 'children'):
        return 0
    i = 0
    for child in tag.children:
        if child.name is not None:
            i = i + 1
    return i


MIN_CHILDS = 2


# unknown_nodes = []


def parse_data(data):
    # global unknown_nodes
    data = data.replace('\n', '').replace('\r', '')
    soup = bs4(data, "html.parser")
    child_count = get_children_count(soup)
    if child_count < MIN_CHILDS:  # why 2 no idea
        for each_child in soup.children:
            if get_children_count(each_child) >= MIN_CHILDS:
                soup = each_child
                break
    # child set
    all_childs = []
    for each_child in soup.children:
        if each_child.name is not None:
            all_childs.append(each_child)

    all_nodes = start_traverse(all_childs)
    for i, node in enumerate(all_nodes):
        LOG("Type: " + str(i) + ": " + node.type)
        if 'unknown' in node.type:
            pass
            # unknown_nodes.append(node)
    return all_nodes, all_childs


class Node(object):
    def __init__(self, child_tags, tag, type):
        self.child_tags = child_tags
        self.tag = tag
        self.type = type

    def __str__(self):
        x = self.type if self.type is not None else ''
        return x + ' c: ' + str(len(self.child_tags))

    def __repr__(self):
        return self.__str__()


def traverse_till_this_state(childs, traverse_from, state):
    consumed = 0
    state_tags = []
    LOG('traversing on state ' + state + ' traverse_from: ' + str(traverse_from))
    while True:
        if traverse_from + consumed >= len(childs):
            break
        child = childs[traverse_from + consumed]
        child_name = str(child.name) if child.name is not None else ''
        LOG('child_name ' + child_name)
        child_class = []
        try:
            child_class = child['class']
        except Exception as e:
            pass
        if state == 'top_summary_box' and child_name == 'p':
            LOG('state is top_summary_box but child is p so skipping this')
            break
        elif child_name == 'p' or child_name == 'ul' or child_name == 'dl':
            LOG('p or ul or dl :' + child_name)
            consumed = consumed + 1
            state_tags.append(child)
        elif child_name == 'table' and u'haudio' in child_class:
            LOG('table haudio')
            state_tags.append(child)
            consumed = consumed + 1
        elif child_name == 'table' and (
                            len(child_class) <= 0 or u'wikitable' in child_class or u'sistersitebox' in child_class):
            LOG(
                'consuming this unidenfitied table as it has no class or wikitable class or has sistersitebox which is like a message in the middle of page')
            state_tags.append(child)
            consumed += 1
        elif child_name == 'div' and u'toc' not in child_class:
            LOG('in div ')
            state_tags.append(child)
            consumed += 1
        else:
            break  # this assumes state will change if the tag name is not p or ul
            # or break if only need to consume just one node
    LOG("consumed in temp: " + str(consumed))

    return state_tags, consumed


# assuming all_childs every child has a tag name
def start_traverse(all_childs):
    nodes = []
    consumed = 0
    while True:
        print('----------------------')
        if consumed >= len(all_childs):
            return nodes
        else:
            child = all_childs[consumed]
            child_tag_name = str(child.name)
            child_id = ''
            child_class = ''
            try:
                child_id = child['id']
            except Exception as e:
                pass
            try:
                child_class = child['class']
            except Exception as e:
                pass
            LOG("Current child_tag_name: " + child_tag_name + " at node: " + str(consumed))
            if child_tag_name == 'h2' or child_tag_name == 'h3':
                LOG('on h2 or h3: ' + child_tag_name)
                first_span_child_id = None
                if hasattr(child, 'children'):
                    LOG('h2 has first span')
                    t_x_C = list(child.children)[0]
                    first_span_child_id = str(t_x_C['id']).lower() if t_x_C['id'] is not None else None
                if first_span_child_id is not None:
                    LOG("first_span_child_id is " + first_span_child_id)
                    c_child_tags, current_consumed = traverse_till_this_state(all_childs, consumed + 1,
                                                                              first_span_child_id)
                    node = Node(c_child_tags, child, first_span_child_id)
                    nodes.append(node)
                    LOG("current_consumed: " + str(current_consumed))
                    if current_consumed == 0:
                        LOG("could not find any nodes for this child meh!!! moving on to avoid infinite loop x1", 2)
                        consumed = consumed + 1
                    else:
                        LOG("increasing consumed by: " + str(current_consumed))
                        consumed = consumed + current_consumed + 1  # for the already read one
                else:
                    LOG("first_span_child_id is None increasing consumed to avoid infinite loop meh x1.1", 2)
                    consumed += 1
            elif child_tag_name == 'div':
                LOG("on div at: " + str(consumed))

                if child_id == u'toc':
                    LOG("found toc on node " + str(consumed))
                    node = Node([], child, 'toc')
                    nodes.append(node)
                elif u'reflist' in child_class:
                    LOG("found reference list on node " + str(consumed))
                    node = Node([], child, 'reflist')
                    nodes.append(node)
                else:
                    node = Node([], child, 'unknown_div')
                    LOG("unkonwn div, meh !!! x1.5", 2)
                consumed += 1
            elif child_tag_name == 'p':
                LOG("on top_summary at node " + str(consumed))
                c_child_tags, current_consumed = traverse_till_this_state(all_childs, consumed+1, 'top_summary')
                node = Node(c_child_tags, child, 'top_summary')
                nodes.append(node)
                if current_consumed == 0:
                    LOG("could not find any nodes for this child meh!!! moving on to avoid infinite loop x2", 2)
                    consumed += 1
                else:
                    consumed = consumed + current_consumed  # no +1 as it is not already consumed here
            elif child_tag_name == 'table':
                if u'infobox' in child_class and u'vevent' in child_class:
                    LOG("found top_summary_box on " + str(consumed))
                    node = Node([], child, 'top_summary_box')
                    nodes.append(node)
                    consumed += 1
                    LOG("might have few more child nodes trying to consume it")
                    c_child_tags, current_consumed = traverse_till_this_state(all_childs, consumed + 1,
                                                                              'top_summary_box')
                    if current_consumed >= 1:
                        consumed = consumed + current_consumed + 1
                        node.child_tags = c_child_tags
                elif u'tracklist' in child_class:
                    LOG("found tracklist on " + str(consumed))
                    node = Node([], child, 'tracklist')
                    nodes.append(node)
                    consumed += 1
                elif u'biography' in child_class:
                    LOG("found biography on " + str(consumed))
                    node = Node([], child, 'biography_table')
                    nodes.append(node)
                    consumed += 1
                else:
                    if u'metadata' in child_class:
                        LOG("a unknown table but was asked ignore it as it is a wikipedia table " + str(child_class))
                        consumed += 1
                        node = Node([], child, 'metadata')
                        nodes.append(node)
                    else:
                        LOG("an unknown table type !!!! moving on to avoid infinite loop x3", 2)
                        node = Node([], child, 'unknown_table')
                        nodes.append(node)
                        consumed += 1
            else:
                LOG('this node is skipped just increasing consumed to avoid infinite loop x4', 2)
                node = Node([], child, 'unknown_node')
                consumed += 1
    return nodes
