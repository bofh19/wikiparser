
import json
import re

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from parse1.models import *


def print_movies_list():
    movie_count = 0
    tables = Table.objects.all()
    for table in tables:
        print (table.table_movie_list.givenWikiTitle)
        rows = table.row_set.all()
        for row in rows:
            movie_count += 1
            for col in row.columndata_set.all():
                print (movie_count, col.col_heading_id.getText(), ":", col.getText())
            print ("------------------")


def get_movies_list():
    movies = []
    for count, col in enumerate(ColumnData.objects.all()):
        col_heading = col.col_heading_id.getText().lower();
        if ('title' in col_heading or 'movie' in col_heading):
            movies.extend(col.getWikiPageTitle())
            print (count, " ",)
    return movies


def parse_movies():
    for count, movie in enumerate(get_movies_list()):
        print ("parsing movie : ", count, movie)
        parse_movie(movie)


def parse_movie(x):
    # x="Sampoorna_Ramayanam_(1971_film)"
    x = LinkToWikiLinkData.objects.get(wiki_link__link_title=x)
    if (x.title_data is None):
        return
    data = x.title_data.data_raw.replace('\n', '')
    soup = BeautifulSoup(data)
    z = soup.find_all('table', attrs={'class': 'infobox vevent'})
    if (len(z) <= 0):
        return
    else:
        info_table = z[0]
    for tr in info_table.find_all('tr'):
        ths = tr.find_all('th')
        tds = tr.find_all('td')
        if (len(ths) == 1 and len(tds) == 1):
            a_s = tds[0].find_all('a')
            if (len(a_s) >= 2):
                for a in a_s:
                    info_table = InfoTable()
                    info_table.wiki_link = x.wiki_link
                    info_table.col1 = ths[0]
                    info_table.col2 = a
                    info_table.save()
            info_table = InfoTable()
            info_table.wiki_link = x.wiki_link
            info_table.col1 = ths[0]
            info_table.col2 = tds[0]
            info_table.save()
        else:
            info_table = InfoTable()
            info_table.wiki_link = x.wiki_link
            info_table.col1 = tr
            info_table.save()
