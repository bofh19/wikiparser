from urllib.request import urlopen
import json
import re
from collections import defaultdict
from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from parse1.models import *

url = "http://en.wikipedia.org/w/api.php?action=parse&page=List_of_Telugu_films_of_the_1930s&format=json&prop=text"

# this is old identify movies that can be ignored

def getMoviesForThisTitle(title="List_of_Telugu_films_of_the_1930s"):
    try:
        mList = MovieListMain.objects.get(givenWikiTitle=title)
        print ("MovieList Data exists returning that")
    except Exception as e:
        print ("Movie List Data does not exist Creating new one", e)
        mList = MovieListMain()
        mList.givenWikiTitle = title
        mList.save()
        url = "http://en.wikipedia.org/w/api.php?action=parse&page=" + title + "&format=json&prop=text"
        data = urlopen(url).read()
        mList.raw_data = data
        mList.save()
    getMovieColumnHeadings(mList)


def getMovieColumnHeadings(mList):
    fj = json.loads(mList.raw_data)
    soup = BeautifulSoup(fj['parse']['text']['*'])
    outtable = soup.find_all('table', attrs={'class': 'wikitable'})
    if (len(outtable) <= 0):
        print ("Error: lenght of outtable is less than 0")
        return
    if (len(outtable) > 1):
        print ("Error: lenght of outtable is more than 1")
        return
    for table in outtable:
        t = Table()
        t.table_movie_list = mList
        t.save()
        rows = table.find_all('tr')
        try:
            throw = rows[0]
            for th in throw.find_all('th'):
                try:
                    colHeading = ColumnHeading.objects.get(col_raw=str(th))
                    print ("ColumnHeading", th, " exist ")
                except Exception as e:
                    print ("ColumnHeading Does not exist creating ", e)
                    colHeading = ColumnHeading()
                    colHeading.col_raw = th
                    colHeading.save()
                t.cols.add(colHeading)
                t.save()
        except Exception as e:
            print ("Error while trying to save column headings ", e)
            return
        getTableRowData(t, table)


def getTableRowData(t, table):
    rows = table.find_all('tr')
    try:
        rows.remove(rows[0])
    except Exception as e:
        print ("Error while trying to remove heading column")
    for row in rows:
        tds = row.find_all('td')
        if (len(tds) >= len(t.cols.all()) or len(tds) > 1):
            mRow = Row()
            mRow.table_movie_list = t
            mRow.row_raw = str(row)
            mRow.save()
            if (len(tds) > len(t.cols.all())):
                print ("Invalid Row Saving on InvalidRow Model ", " Tds Length ", len(tds), " cols count ", len(
                    t.cols.all()))
                i_row = InvalidRow()
                i_row.row_raw = str(row)
                i_row.movie_list = t.table_movie_list
                i_row.save()
            else:
                for count, td in enumerate(tds):
                    c_data = ColumnData()
                    c_data.row_id = mRow
                    c_data.col_heading_id = t.cols.all()[count]
                    c_data.col_raw = str(td)
                    c_data.save()
        else:
            print ("Invalid Row Saving on InvalidRow Model ", " Tds Length ", len(tds), " cols count ", len(t.cols.all()))
            i_row = InvalidRow()
            i_row.row_raw = str(row)
            i_row.movie_list = t.table_movie_list
            i_row.save()


def get_found_movies():
    rows = Row.objects.all()
    for row in rows:
        cols = row.columndata_set.all()
        print ("-------")
        for col in cols:
            print (col.col_heading_id, " : ", col.getText(), "(", col.getHref(), ")")


# foundMovie = Movie()
# 			x = row.find_all('td')[0]
# 			foundMovie.name = x.get_text()
# 			print x.get_text(),
# 			try:
# 				href= x.find_all('a')[0]['href']
# 				foundMovie.wiki_link = href
# 				print href
# 			except Exception, e:
# 				print e
# 			try:
# 				foundMovie.save()
# 			except Exception, e:
# 				print "Unable to Save",e
# 		except Exception, e:
# 			print e

def table_to_dict(table):
    #result = defaultdict(lambda: defaultdict(unicode))
    result = defaultdict(lambda: defaultdict())
    for row_i, row in enumerate(table.find_all('tr')):
        for col_i, col in enumerate(row.find_all('td')):
            colspan = 1
            rowspan = 1
            try:
                colspan = int(col['colspan'])
            except Exception as e:
                pass
            try:
                rowspan = int(col['rowspan'])
            except Exception as e:
                pass
            col_data = str(col)
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    result[i][j] = col_data
    return result


def iter_2d_dict(dct):
    for i, row in sorted(dct.items()):
        cols = []
        for j, cl in sorted(row.items()):
            cols.append(cl)
        yield cols
