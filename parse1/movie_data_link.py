from parse1.models import *


def get_movies_from_col_data():
    movie_hrefs = []
    for d in ColumnData.objects.all():
        if (('title' in d.col_heading_id.getText().lower())
            or ('movie' in d.col_heading_id.getText().lower())):
            movie_hrefs.extend(d.getWikiPageTitle())
    return movie_hrefs
