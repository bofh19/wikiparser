from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.datastructures import MultiValueDictKeyError
# from sorl.thumbnail import get_thumbnail

from django.http import HttpResponse
import json
import random

from parse1.models import *

mimetype = 'application/json'
import gc


def get_wiki_link_raw(request, data_id):
    wiki_link_data_obj = WikiLinkData.objects.get(id=data_id)
    return render_to_response('parse1/wiki_link_raw_view.html', {'ob': wiki_link_data_obj},
                              context_instance=RequestContext(request))

def get_wiki_link_divided_raw(request,data_id):
    wiki_link_divided_data_obj = WikiLinkDataDivided.objects.get(id=data_id)
    return render_to_response('parse1/wiki_link_divided_raw_view.html',{'ob': wiki_link_divided_data_obj},
                              context_instance=RequestContext(request))


def api_search_html(request):
    results = []
    if request.POST:
        q = request.POST.get('term', '')
        print(q)
        if (len(q) > 3):
            results = getFoundColResults(q)
            print(results)
    return render_to_response(
        'parse1/search_details.html',
        {'results': results},
        context_instance=RequestContext(request)
    )


import time


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def api_search_all(request):
    millis1 = int(round(time.time() * 1000))
    q = request.GET.get('term', '')
    print("got query", q)
    if (len(q) < 3):
        results = {'term': q}
        data = json.dumps(results)
        return HttpResponse(data, mimetype)

    results = getFoundColResultsAPI(q)
    millis2 = int(round(time.time() * 1000))
    tt = millis2 - millis1
    print(millis1, millis2, tt)
    res2 = {}
    res2['meta'] = {'time': str(tt) + "ms"}
    res2['objects'] = results
    data = json.dumps(res2)
    print("return result ", gc.collect())
    print("request ip ", get_client_ip(request))
    return HttpResponse(data, mimetype)


def getFoundColResultsAPI(q):
    print("in found cols results api")
    foundCols = ColumnData.objects.filter(col_raw__icontains=q)
    # foundCols = ColumnData.objects.filter(col_raw__iregex=r"\y{0}\y".format(q))
    results = []
    for col in foundCols:
        print('in cols loop', col.getWikiPageTitle())
        this_col_row_col_set = col.row_id.columndata_set.all()
        col_obj = {}
        if (len(col.row_id.getMovieCol()) == 1):
            if (len((col.row_id.getMovieCol()[0]).getWikiPageTitle()) >= 1):
                col_obj['movie_details'] = getWikiMovieDataAPI((col.row_id.getMovieCol()[0]).getWikiPageTitle()[0])
            else:
                print("failed col.row_id.getMovieCol.getWikiPageTitle", (col.row_id.getMovieCol()[0]).getWikiPageTitle())
        else:
            print("failed col.row_id.getMovieCol ", col.row_id.getMovieCol())
        for row_col in this_col_row_col_set:
            print('in rows loop')
            col_obj[row_col.col_heading_id.getText()] = row_col.getText()
            col_obj['list'] = row_col.row_id.table_movie_list.table_movie_list.givenWikiTitle
            print('collect in rows', gc.collect())
        print('collect on cols', gc.collect())
        results.append(col_obj)
    print("out ", gc.collect())
    return list(reversed(results))


def getWikiMovieDataAPI(href):
    href = href.replace(' ', '_')
    infos = InfoTable.objects.filter(wiki_link__link_title=href)
    res = {}
    for info in infos:
        if (len(info.getCol1Text()) > 0):
            res[info.getCol1Text()] = info.getCol2Text()
    return res


def getFoundColResults(q):
    foundCols = ColumnData.objects.filter(col_raw__icontains=q)
    # foundCols = ColumnData.objects.filter(col_raw__iregex=r"\y{0}\y".format(q))
    results = []
    for col in foundCols:
        this_col_row_col_set = col.row_id.columndata_set.all()
        col_obj = {}
        col_obj['movie_details'] = []
        found_hrefs = []
        for row_col in this_col_row_col_set:
            # x={}
            # x['heading'] = row_col.col_heading_id.getText()
            # x['val'] = row_col.getText()
            # col_array.append(x)
            # col_obj[row_col.col_heading_id.getText()] = row_col.getText()
            # col_obj['list'] = row_col.row_id.table_movie_list.table_movie_list.givenWikiTitle
            for href in row_col.getWikiPageTitle():
                found_hrefs.append(href)
            found_hrefs = list(set(found_hrefs))
            if (
                            'title' in row_col.col_heading_id.getText().lower() or 'movie' in row_col.col_heading_id.getText().lower()):
                for href in found_hrefs:
                    col_obj['movie_details'].extend(getWikiMovieData(href))
        results.append(col_obj)

    return list(reversed(results))


def getWikiMovieData(href):
    href = href.replace(' ', '_')
    infos = InfoTable.objects.filter(wiki_link__link_title=href)
    res = []
    for info in infos:
        res.append((info.getCol1Text(), info.getCol2Text()))
    return res


def getWikiLinkData(href):
    href = href.replace(' ', '_')
    return_data = []
    try:
        wikilink = WikiLink.objects.get(link_title=href)
    except Exception as e:
        return []
    for x in wikilink.wiki_link.all():
        obj = {}
        obj['href'] = x.wiki_link.link_title
        while (x.is_title_redirect):
            try:
                x = x.is_title_rected_to.wiki_link.all()[0]
            except Exception as e:
                break
        obj['data'] = x.title_data.data_raw.replace('\n', '')
        return_data.append(obj)
    return return_data
