from django.contrib import admin

# Register your models here.
from parse1.models import *
from django.utils.html import format_html

#admin.site.register(Movie)

admin.site.register(ColumnHeading)
admin.site.register(MovieListMain)
admin.site.register(Table)
admin.site.register(Row)
admin.site.register(ColumnData)
#admin.site.register(InvalidRow)



#admin.site.register(WikiLink)
#admin.site.register(LinkToWikiLinkData)

#admin.site.register(InfoTable)


class InlineHtmlWikiLinkData(admin.ModelAdmin):
    model = WikiLinkData
    readonly_fields = ('data_html_view',)

    def data_html_view(self, instance):
        return format_html('<iframe width="100%" style="min-height: 1000px" src="/api/raw_data/' + str(
            int(instance.id)) + '/"></iframe>')

    data_html_view.short_description = "html view"


admin.site.register(WikiLinkData, InlineHtmlWikiLinkData)


class InlineWikiLinkDataDivided(admin.ModelAdmin):
    model = WikiLinkDataDivided
    readonly_fields = ('parent', 'wiki_link', 'wiki_link_link', 'data_html_view',)

    def parent(self, instance):
        if instance.parent:
            return WikiLinkDataDivided.objects.get(id=instance.parent_id)
        else:
            return ''

    def wiki_link(self, instance):
        return WikiLink.objects.get(id=instance.wiki_link_id).link_title

    def wiki_link_link(self, instance):
        all_links = LinkToWikiLinkData.objects.filter(wiki_link=instance.wiki_link_id)
        for each_link in all_links:
            if each_link.title_data:
                return format_html(
                    '<a href="/admin/parse1/wikilinkdata/{0}/change" target="_blank">click here {1} </a>'.format(
                        each_link.title_data_id, each_link.title_data_id))
        return ''

    def data_html_view(self, instance):
        return format_html('<iframe width="100%" style="min-height: 1000px" src="/api/divided_raw_data/' + str(
            int(instance.id)) + '/"></iframe>')

    data_html_view.short_description = "html view"
    wiki_link_link.short_description = "link to full data"

#admin.site.register(WikiLinkDataDivided, InlineWikiLinkDataDivided)
