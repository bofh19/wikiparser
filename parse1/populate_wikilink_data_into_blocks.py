__author__ = 'skarumuru'

from parse1.models import *

from parse1.parse_page_data import *

def divide_wikilink_data_into_blocks():
    all_data_links = LinkToWikiLinkData.objects.all()
    for each_data_link in all_data_links:
        if not each_data_link.is_title_redirect:
            data = each_data_link.title_data.data_raw
            wiki_link = each_data_link.wiki_link
            nodes, childs = parse_data(data)
            for node in nodes:
                node_db = WikiLinkDataDivided()
                node_db.wiki_link = wiki_link
                node_db.type = node.type
                node_db.tag_data = str(node.tag)
                node_db.save()
                for each_child_item in node.child_tags:
                    child_node_db = WikiLinkDataDivided()
                    child_node_db.wiki_link = wiki_link
                    child_node_db.type = node.type
                    child_node_db.tag_data = str(each_child_item)
                    child_node_db.parent = node_db
                    child_node_db.save()