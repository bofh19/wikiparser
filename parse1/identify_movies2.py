# identify_movies2.py

from urllib.request import urlopen
import json
import re

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from parse1.models import *

url = "http://en.wikipedia.org/w/api.php?action=parse&page=List_of_Telugu_films_of_the_1930s&format=json&prop=text"

# this file is used to dump all data from wikipedia only gets the list of movies
# into the database

def get_all_movies():
    for i in range(3, 10):
        q = "List_of_Telugu_films_of_the_19" + str(i) + "0s"
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)
    for i in range(0, 9):
        q = "List_of_Telugu_films_of_194" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)
    for i in range(0, 9):
        q = "List_of_Telugu_films_of_195" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)
    for i in range(0, 9):
        q = "List_of_Telugu_films_of_196" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)

    for i in range(0, 4):
        q = "List_of_Telugu_films_of_197" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)
    q = "List_of_Telugu_films_of_1979"
    print("GETTING FOR : ", q)
    getMoviesForThisTitle2(title=q)

    for i in range(0, 9):
        q = "List_of_Telugu_films_of_198" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)

    for i in range(0, 9):
        q = "List_of_Telugu_films_of_199" + str(i)
        print("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)

    for i in range(0, 9):
        q = "List_of_Telugu_films_of_200" + str(i)
        print ("GETTING FOR : ", q)
        getMoviesForThisTitle2(title=q)

    for i in range(0, 5):
        try:
            q = "List_of_Telugu_films_of_201" + str(i)
            print ("GETTING FOR : ", q)
            getMoviesForThisTitle2(title=q)
        except Exception as e:
            print(e)


def getMoviesForThisTitle2(title="List_of_Telugu_films_of_the_1930s"):
    try:
        mList = MovieListMain.objects.get(givenWikiTitle=title)
        print ("MovieList Data exists returning that")
    except Exception as e:
        print ("Movie List Data does not exist Creating new one", e)
        mList = MovieListMain()
        mList.givenWikiTitle = title
        mList.save()
        url = "http://en.wikipedia.org/w/api.php?action=parse&page=" + title + "&format=json&prop=text"
        data = urlopen(url).read().decode('utf8')
        mList.raw_data = data
        mList.save()
    getMovieColumnHeadings(mList)


def getMovieColumnHeadings(mList):
    fj = json.loads(mList.raw_data)
    soup = BeautifulSoup(fj['parse']['text']['*'])
    outtable_all = soup.find_all('table', attrs={'class': 'wikitable'})
    outtable = []
    for each_table in outtable_all:
        if 'succession-box' not in each_table['class']:
            outtable.append(each_table)
    if (len(outtable) <= 0):
        print ("Error: lenght of outtable is less than 0")
        return
    if (len(outtable) > 1):
        print ("Error: lenght of outtable is more than 1 trying on all")
    for table in outtable:
        t = Table()
        t.table_movie_list = mList
        t.save()
        ths = get_table_as_list(table, th=True)
        print (ths)
        for th in ths[0]:
            print (th)
            try:
                colHeading = ColumnHeading.objects.get(col_raw=th)
                print ("column heading ", th, "exist")
            except Exception as e:
                print ("ColumnHeading Does not exist creating ", e)
                colHeading = ColumnHeading()
                colHeading.col_raw = th
                colHeading.save()
            t.cols.add(colHeading)
            print (th, "saved")
            t.save()
        getTableRowData(t, table)


def getTableRowData(t, table):
    dt = get_table_as_list(table)
    for row in dt:
        mRow = Row()
        mRow.table_movie_list = t
        mRow.row_raw = ''.join(row)
        mRow.save()
        chs = t.cols.order_by(t.cols.through._meta.db_table + '.id')
        for count, col in enumerate(row):
            c_data = ColumnData()
            c_data.row_id = mRow
            try:
                c_data.col_heading_id = chs[count]
            except Exception as e:
                c_data.col_heading_id = getInvalidColumnHeading()
            c_data.col_raw = col
            c_data.save()
    rows = table.find_all('tr')


def getInvalidColumnHeading():
    try:
        return ColumnHeading.objects.get(col_raw="INVALID_OR_UNKNOWN")
    except Exception as e:
        col = ColumnHeading()
        col.col_raw = "INVALID_OR_UNKNOWN"
        col.save()
        return col


def get_found_movies():
    rows = Row.objects.all()
    for row in rows:
        cols = row.columndata_set.all()
        for col in cols:
            print (col.col_heading_id, " : ", col.getText(), "(", col.getHref(), ")")


# foundMovie = Movie()
# 			x = row.find_all('td')[0]
# 			foundMovie.name = x.get_text()
# 			print x.get_text(),
# 			try:
# 				href= x.find_all('a')[0]['href']
# 				foundMovie.wiki_link = href
# 				print href
# 			except Exception, e:
# 				print e
# 			try:
# 				foundMovie.save()
# 			except Exception, e:
# 				print "Unable to Save",e
# 		except Exception, e:
# 			print e

def table_to_dict(table):
    #result = defaultdict(lambda: defaultdict(unicode))
    result = defaultdict(lambda: defaultdict())
    for row_i, row in enumerate(table.find_all('tr')):
        for col_i, col in enumerate(row.find_all('td')):
            colspan = 1
            rowspan = 1
            try:
                colspan = int(col['colspan'])
            except Exception as e:
                pass
            try:
                rowspan = int(col['rowspan'])
            except Exception as e:
                pass
            col_data = str(col)
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    result[i][j] = col_data
    return result


from collections import defaultdict


def table_to_dict_th(table):
    #result = defaultdict(lambda: defaultdict(unicode))
    result = defaultdict(lambda: defaultdict())
    for row_i, row in enumerate(table.find_all('tr')):
        for col_i, col in enumerate(row.find_all('th')):
            colspan = 1
            rowspan = 1
            try:
                colspan = int(col['colspan'])
            except Exception as e:
                pass
            try:
                rowspan = int(col['rowspan'])
            except Exception as e:
                pass
            col_data = str(col)
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    if (colspan > 1):
                        result[i][j] = col_data + "_col_" + str(j)
                    else:
                        result[i][j] = col_data
    return result


def iter_2d_dict(dct):
    for i, row in sorted(dct.items()):
        cols = []
        for j, col in sorted(row.items()):
            cols.append(col)
        yield cols


def get_table_as_list(table, th=False):
    if (th):
        return list(iter_2d_dict(table_to_dict_th(table)))
    else:
        return list(iter_2d_dict(table_to_dict(table)))
