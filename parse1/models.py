from django.db import models

# Create your models here.
from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

# import urlparse
from urllib import parse as urlparse


class ColumnHeading(models.Model):
    col_raw = models.CharField(max_length=255, unique=True)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)

    def getText(self):
        """
		returns display text value for col_raw
		"""
        try:
            soup = BeautifulSoup(self.col_raw, "html.parser")
            return soup.get_text().replace('\n', ' ')
        except Exception as e:
            return self.col_raw

    def __unicode__(self):
        return self.getText()

    def __str__(self):
        return self.getText()


class MovieListMain(models.Model):
    givenWikiTitle = models.CharField(max_length=255, unique=True)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)
    raw_data = models.TextField()

    def __unicode__(self):
        return self.givenWikiTitle

    def __str__(self):
        return self.givenWikiTitle


class Table(models.Model):
    table_movie_list = models.ForeignKey(MovieListMain)
    cols = models.ManyToManyField(ColumnHeading)

    def __unicode__(self):
        return str(self.id) + " / " + self.table_movie_list.givenWikiTitle

    def __str__(self):
        return str(self.id) + " / " + self.table_movie_list.givenWikiTitle


class Row(models.Model):
    row_raw = models.TextField()
    table_movie_list = models.ForeignKey(Table)

    def __unicode__(self):
        return str(self.id) + " / ( " + str(
            self.table_movie_list.id) + " / " + self.table_movie_list.table_movie_list.givenWikiTitle + " )"

    def __str__(self):
        return str(self.id) + " / ( " + str(
            self.table_movie_list.id) + " / " + self.table_movie_list.table_movie_list.givenWikiTitle + " )"

    cols = None

    def init_cols(self):
        if (self.cols is None):
            self.cols = self.columndata_set.all()

    # all below columns returns None
    # or an array of cols
    def getMovieCol(self):
        self.init_cols()
        i_text = ['title', 'movie']
        return self.get_the_cols(i_text)

    def getCastCol(self):
        self.init_cols()
        i_text = 'cast'
        return self.get_the_cols(i_text)

    def getNotesCol(self):
        self.init_cols()
        i_text = 'notes'
        return self.get_the_cols(i_text)

    def getGenreCol(self):
        self.init_cols()
        i_text = 'genre'
        return self.get_the_cols(i_text)

    def getDirectorCol(self):
        self.init_cols()
        i_text = 'director'
        return self.get_the_cols(i_text)

    def getMusicCol(self):
        self.init_cols()
        i_text = 'music'
        return self.get_the_cols(i_text)

    def getRemarksCol(self):
        self.init_cols()
        i_text = 'remarks'
        return self.get_the_cols(i_text)

    def getOpening0Col(self):
        self.init_cols()
        i_text = 'opening_col_0'
        return self.get_the_cols(i_text)

    def getOpening1Col(self):
        self.init_cols()
        i_text = 'opening_col_1'
        return self.get_the_cols(i_text)

    def getStudioCol(self):
        self.init_cols()
        i_text = ['studio', 'house']
        return self.get_the_cols(i_text)

    def get_the_cols(self, i_text):
        the_cols = []
        for col in self.cols:
            if (self.identify_this_col(col, i_text)):
                the_cols.append(col)
        return the_cols

    def identify_this_col(self, col, text):
        if (isinstance(text, list)):
            if any(col.col_heading_id.getText().lower() in s for s in text):
                return True
        else:
            if (text.lower() in col.col_heading_id.getText().lower()):
                return True
        return False


class ColumnData(models.Model):
    row_id = models.ForeignKey(Row)
    col_heading_id = models.ForeignKey(ColumnHeading)
    col_raw = models.TextField()

    def getHref(self):
        try:
            x = []
            soup = BeautifulSoup(self.col_raw, "html.parser")
            for ea in soup.find_all('a'):
                try:
                    if "index.php" not in ea['href']:
                        x.append(ea['href'])
                except Exception as e:
                    pass
            return x
        except Exception as e:
            return []
        return []

    def getWikiPageTitle(self):
        wt = []
        for ea in self.getHref():
            s = ea.split('/')
            if (len(s) > 0):
                if "index.php" not in s[len(s) - 1] and '#' != s[-1][0]:
                    wt.append(s[len(s) - 1])
        try:
            for ea in self.getHref():
                parsed = urlparse.parse_qs(urlparse.urlparse(ea).query)
                wt.extend(parsed['title'])
            wt = list(set(wt))
        except Exception as e:
            pass
        return wt

    def getText(self):
        """
        returns text data from col_raw
        """
        try:
            soup = BeautifulSoup(self.col_raw, "html.parser")
            return soup.get_text().replace("\n", " ")
        except Exception as e:
            return self.col_raw

    def __unicode__(self):
        return self.col_heading_id.getText() + " / " + self.getText()

    def __str__(self):
        return self.col_heading_id.getText() + " / " + self.getText()


class InvalidRow(models.Model):
    row_raw = models.TextField()
    movie_list = models.ForeignKey(MovieListMain)


class Movie(models.Model):
    name = models.CharField(max_length=255, unique=True)
    wiki_link = models.CharField(verbose_name=("Found Link"), max_length=255, unique=True)
    redlink = models.BooleanField(default=False)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def getWikiTitle(self):
        """
		returns wiki pedia title from wiki link despite the redlink flag
		needs implementation
		"""
        return self.wiki_link


class WikiLinkData(models.Model):
    data_raw = models.TextField()

    def __unicode__(self):
        x = ''
        for t in self.linktowikilinkdata_set.all():
            x = x + str(t)
        return x

    def __str__(self):
        x = ''
        for t in self.linktowikilinkdata_set.all():
            x = x + str(t)
        return x


class WikiLink(models.Model):
    link_title = models.CharField(max_length=255, unique=True)
    data_saved = models.BooleanField(default=False)
    has_failed = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        self.link_title = self.link_title.replace(' ', '_')
        super(WikiLink, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.link_title + " / " + str(self.data_saved)

    def __str__(self):
        return self.link_title + " / " + str(self.data_saved)


class WikiLinkDataDivided(models.Model):
    wiki_link = models.ForeignKey(WikiLink)
    type = models.CharField(max_length=512)
    tag_data = models.TextField(blank=True, null=True)
    parent = models.ForeignKey('WikiLinkDataDivided', blank=True, null=True)

    def __unicode__(self):
        return self.wiki_link.link_title + ' - ' + self.type

    def __str__(self):
        return WikiLink.objects.get(id=self.wiki_link_id).link_title + ' - ' + self.type


class LinkToWikiLinkData(models.Model):
    wiki_link = models.ForeignKey(WikiLink, unique=True, related_name='wiki_link')
    title_data = models.ForeignKey(WikiLinkData, null=True, blank=True)
    is_title_redirect = models.BooleanField(default=False)
    is_title_rected_to = models.ForeignKey(WikiLink, null=True, blank=True, related_name='redirect_link')

    def __unicode__(self):
        if (self.is_title_redirect):
            return self.wiki_link.link_title + " -> " + self.is_title_rected_to.link_title
        else:
            return self.wiki_link.link_title

    def __str__(self):
        if (self.is_title_redirect):
            return self.wiki_link.link_title + " -> " + self.is_title_rected_to.link_title
        else:
            return self.wiki_link.link_title


class InfoTable(models.Model):
    wiki_link = models.ForeignKey(WikiLink, related_name='info_wiki_link')
    col1 = models.TextField()
    col2 = models.TextField(blank=True, null=True)

    def getCol1Text(self):
        if (self.col1 is not None):
            soup = BeautifulSoup(self.col1, "html.parser")
            return soup.get_text()
        return ""

    def getCol2Text(self):
        if (self.col2 is not None):
            soup = BeautifulSoup(self.col2, "html.parser")
            if (self.getCol1Text().lower() in "starring"):
                if (len(soup.find_all('a')) > 0):
                    return ", ".join(s.get_text() for s in soup.find_all('a'))

            return soup.get_text()
        return ""

    def getText(self):
        try:
            text = self.wiki_link.link_title + " / "
            if self.col1 is not None:
                soup = BeautifulSoup(self.col1, "html.parser")
                text += soup.get_text()
            if self.col2 is not None:
                soup = BeautifulSoup(self.col2, "html.parser")
                text += " / "
                text += soup.get_text()
            return text
        except Exception as e:
            text = ""
            if self.col1 is not None:
                text += self.col1
            if self.col2 is not None:
                text += self.col2
            return text

    def __unicode__(self):
        return self.getText()

    def __str__(self):
        return self.getText()
