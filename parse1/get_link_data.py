from urllib.request import urlopen
import json
import re

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from parse1.models import *

url = "http://en.wikipedia.org/w/api.php?action=parse&format=json&page=Tamanna_Bhatia&redirects=&prop=text"


# this file dumps all the href data from wikipedia into database

def get_t_hrefs():
    t_hrefs = []
    for x in ColumnData.objects.all():
        t_hrefs.extend(x.getWikiPageTitle())
    t_hrefs = list(set(t_hrefs))
    return t_hrefs


def start_execution():
    t_hrefs = get_t_hrefs()
    for count, href in enumerate(t_hrefs):
        print("/////// ", count, "  /////////// ", href, " ////////////")
        print(get_link_data(href))


def get_data_from_url(wlink):
    """
	Input:
		wlink - WikiLink model
	returns:
		[boolean,LinkToWikiLinkData,WikiLinkData]
		if boolean is true then other 2 will also follow
		if boolean is false then unable to get data
	"""
    # wlink = WikiLink.objects.get(id=4)
    link_title = wlink.link_title
    url = "http://en.wikipedia.org/w/api.php?action=parse&format=json&page=" + link_title + "&redirects=&prop=text"
    f = urlopen(url)
    data = f.read().decode('utf8')
    json_data = json.loads(data)
    try:
        print("wlink[", wlink, "]", "check for error")
        failed_check = json_data['error']
        print("Wlink[", wlink, "]", "has error ", data)
        w_link_data = WikiLinkData()
        w_link_data.data_raw = data
        w_link_data.save()

        link_to_data = LinkToWikiLinkData()
        link_to_data.wiki_link = wlink
        link_to_data.title_data = w_link_data
        link_to_data.save()

        wlink.has_failed = True
        wlink.data_saved = True
        wlink.save()
        print("wlink[", wlink, "]", "error value saved")
        return [False, ]
    except Exception as e:
        print("wlink ", wlink, "]", "has no error continuing")
        pass

    try:
        print("wlink[", wlink, "]", "check for redirect")
        redirect_to = json_data['parse']['redirects'][0]['to'].replace(' ', '_')
        print("wlink[", wlink, "]", "found redirect ", redirect_to)
        print("wlink[", wlink, "]", "check if redirect_to data exist", redirect_to)
        try:
            print("wlink[", wlink, "]", "trying to get redirect wlink")
            wlink_redirect_to = WikiLink.objects.get(link_title=redirect_to)
        except Exception as e:
            print("wlink[", wlink, "]", "redirect wlink does not exist so creating it")
            wlink_redirect_to = WikiLink()
            wlink_redirect_to.link_title = redirect_to
            wlink_redirect_to.save()
            get_data_from_url(wlink_redirect_to)
            wlink_redirect_to = WikiLink.objects.get(link_title=redirect_to)
        try:
            print("wlink[", wlink, "]", "Got wlink_redirect_to", "[", wlink_redirect_to, "]", wlink_redirect_to.id)
            # print "wlink[",wlink,"]","wlink_redirect_to data saved is",wlink_redirect_to.data_saved,"so just setting redirect"
            link_to_data = LinkToWikiLinkData.objects.get_or_create(wiki_link=wlink)[0]
            link_to_data.wiki_link = wlink
            link_to_data.is_title_redirect = True
            link_to_data.is_title_rected_to = wlink_redirect_to
            link_to_data.save()
            wlink = WikiLink.objects.get(link_title=wlink.link_title)
            wlink.data_saved = True
            wlink.has_failed = False
            wlink.save()
            return [wlink_redirect_to.data_saved, link_to_data, link_to_data.title_data]
        except Exception as e:
            print("wlink[", wlink, "]", "failed while trying to get redirect ", e)
    except Exception as e:
        print("wlink[", wlink, "]", "has no redirect", e)
    print("wlink[", wlink, "]", "just saving text raw data for now it may have internal list ")
    try:
        w_link_data = WikiLinkData()
        raw_data = json_data['parse']['text']['*']
        dammit = UnicodeDammit(raw_data)
        htext = dammit.unicode_markup
        w_link_data.data_raw = htext
        w_link_data.save()
        link_to_data = LinkToWikiLinkData()
        link_to_data.wiki_link = wlink
        link_to_data.title_data = w_link_data
        link_to_data.save()
        wlink.data_saved = True
        wlink.has_failed = False
        wlink.save()
        print("saved[", wlink, "]raw data from parse/text/*")
        return [True, link_to_data, link_to_data.title_data]
    except Exception as e:
        print("FAILED TO SAVE ON JUST RAW DATA OR LINK DATA", e)
        return [False]


def get_link_data(wlink):
    """
	Input:
		wlink - string in underscore format 
	returns:
		LinkToWikiLinkData object 
		either by getting data and saving it 
		or if already exists by just importing it and returning it
	"""
    wlink = wlink.replace(' ', '_')
    try:
        wlink_obj = WikiLink.objects.get(link_title=wlink)
        if (wlink_obj.data_saved):
            try:
                print(wlink_obj, "found and data_saved is true")
                return LinkToWikiLinkData.objects.get(wiki_link=wlink_obj)
            except Exception as e:
                print("Invalid Data in wlink (says data_saved is true but unable to find it )", wlink, e)
                wlink_obj.data_saved = False
                wlink_obj.save()
        else:
            arr = get_data_from_url(wlink_obj)
            wlink_obj.data_saved = arr[0]
            wlink_obj.save()
    except Exception as e:
        print("Unable to find data from wikilink Getting them and saving them", wlink)
        wlink_obj = WikiLink()
        wlink_obj.link_title = wlink
        wlink_obj.save()
        arr = get_data_from_url(wlink_obj)
        wlink_obj.data_saved = arr[0]
        wlink_obj.save()
