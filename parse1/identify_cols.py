__author__ = 'w4rlock'

import json
import re

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from parse1.models import *


# this file is used what ???

def start_identify_each_row():
    rows = Row.objects.all().reverse()
    for row in rows:
        for mcol in row.getMovieCol():
            print('Movie :', mcol.getText(), 'Data :', check_if_data_exists(mcol.getText))

        for mcol in row.getCastCol():
            # for
            print('Cast :', mcol.getText())

        for mcol in row.getGenreCol():
            print('Genre :', mcol.getText())

        for mcol in row.getDirectorCol():
            print('Director :', mcol.getText())

        for mcol in row.getMusicCol():
            print('Music :', mcol.getText())

        for mcol in row.getOpening0Col():
            print('Opening 0 :', mcol.getText())

        for mcol in row.getOpening1Col():
            print('Opening 1 :', mcol.getText())

        for mcol in row.getStudioCol():
            print('Studio :', mcol.getText())


def check_if_data_exists(text):
    text = text.replace(' ', '_')
    try:
        obj = WikiLink.objects.get(link_title__icontains=text)
        if (obj.data_saved):
            return True
        return False
    except Exception as e:
        return False


def check_if_data_exists_href(href):
    pass


def identify_this_col(col):
    col_text = col.getText().lower()
    col_heading_text = col.col_heading_id.getText().lower()
    # print 'identifying on',col_text,col_heading_text,':',
    if ('title' in col_heading_text or 'movie' in col_heading_text):
        # print "May be Movie"
        pass
    elif ('cast' in col_heading_text):
        # print 'May be Cast'
        pass
    elif ('genre' in col_heading_text):
        # print 'May be Genre'
        pass
    elif ('notes' in col_heading_text):
        # print 'May be Notes'
        pass
    elif ('director' in col_heading_text):
        # print 'May be Director'
        pass
    elif ('music' in col_heading_text):
        # print 'May be Music'
        pass
    elif ('remarks' in col_heading_text):
        # print 'May be Remarks'
        pass
    elif ('opening_col_0' in col_heading_text):
        # print 'May be Opening Col 0'
        pass
    elif ('opening_col_1' in col_heading_text):
        # print 'May be Opening Col 1'
        pass
    elif ('studio' in col_heading_text or 'house' in col_heading_text):
        # print 'May be Studio'
        pass
    else:
        print('identifying on', col_text, col_heading_text, ':', )
        print("UNKNOWN")
