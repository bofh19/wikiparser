from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from parse1 import views as p1views
from parse2 import views as p2views
from wikiparse import settings as wikiparse_settings

admin.autodiscover()

urlpatterns = [url(r'^api/search_all/$', p1views.api_search_all),
               url(r'^admin/', include(admin.site.urls)),

               url(r'^api/search/$', p1views.api_search_html),
               url(r'^api/raw_data/(?P<data_id>\d+)/$', p1views.get_wiki_link_raw),
                url(r'^api/WikiPediaLinkDataRaw/(?P<data_id>\d+)/$', p2views.get_wiki_link_data_raw),
               url(r'^api/divided_raw_data/(?P<data_id>\d+)/$', p1views.get_wiki_link_divided_raw),
               ] + static(wikiparse_settings.MEDIA_URL, document_root=wikiparse_settings.MEDIA_ROOT)
